package PropertiesControl;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class PropertyControllerForAdmin extends AnchorPane {
    @FXML
    AnchorPane anchorPane;

    @FXML
    PasswordField passwordField;

    @FXML
    Button buttonYes;

    @FXML
    Button buttonNo;

    private final List<IAdminWindowEvent> listenersProperty = new ArrayList<>();

    public void addListenerProperty(IAdminWindowEvent toAdd) {
        listenersProperty.add(toAdd);
    }

    public static Stage secondaryStage;// = new Stage();
    public static Scene secondaryScene;

    public PropertyControllerForAdmin() {
        initializeAdminWindow();
    }


    public void initializeAdminWindow() {
        FXMLLoader loader2 = new FXMLLoader(getClass().getResource("PropertyControllerForAdmin.fxml"));
        loader2.setRoot(this);
        loader2.setController(this);

        try {
            secondaryScene = new Scene(loader2.load());
        } catch (IOException e) {
            e.printStackTrace();
        }
        secondaryStage = new Stage();
        //secondaryStage.setAlwaysOnTop(true);
        secondaryStage.initModality(Modality.APPLICATION_MODAL);
        secondaryStage.initStyle(StageStyle.UNDECORATED);
        secondaryStage.setResizable(false);
        secondaryStage.setScene(secondaryScene);

        secondaryStage.show();

        passwordField.setOnMouseClicked(mouseEvent -> passwordField.setStyle("-fx-border-color: grey"));
    }

    @FXML
    public void handleEnterPressed(KeyEvent event) {
        String password = "12345";
        if (passwordField.getText().equals(password)) {
            try {
                if (event.getCode().equals(KeyCode.ENTER)) {
                    for (IAdminWindowEvent propertyEvent : listenersProperty)
                        propertyEvent.AdminWindowAccessGranted();
                    secondaryStage.close();
                }
            } catch (Exception ignored) {
            }
        } else if (!passwordField.getText().equals(password)) {
            if (event.getCode().equals(KeyCode.ENTER)) {
                passwordField.setStyle("-fx-border-color: red");
            } else {
                passwordField.setStyle("-fx-border-color: grey");
            }
        }
    }

    @FXML
    public void exit(KeyEvent event) throws IOException {
        if (event.getCode().equals(KeyCode.ESCAPE)) {
            for (IAdminWindowEvent propertyEvent : listenersProperty)
                propertyEvent.AdminWindowAccessDenied();
            secondaryStage.close();
        }
    }

    @FXML
    public void pressButtonYes(ActionEvent event) throws IOException {
        String password = "12345";
        if (passwordField.getText().equals(password)) {
            try {
                for (IAdminWindowEvent propertyEvent : listenersProperty)
                    propertyEvent.AdminWindowAccessGranted();
                secondaryStage.close();
            } catch (Exception ignored) {
            }
        } else if (!passwordField.getText().equals(password)) {
            passwordField.setStyle("-fx-border-color: red");
        }
    }

    @FXML
    public void pressButtonNo(ActionEvent event) throws IOException {
        for (IAdminWindowEvent propertyEvent : listenersProperty)
            propertyEvent.AdminWindowAccessDenied();
        secondaryStage.close();
    }


}

