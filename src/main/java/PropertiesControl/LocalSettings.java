package PropertiesControl;

public class LocalSettings {

    General general = new General();
    Map map = new Map();
    TwoFields db = new TwoFields();
    TwoFields df = new TwoFields();
    ED ed = new ED();
    PC pc = new PC();
    BPSS bpss = new BPSS();
    Sound sound = new Sound();

    public LocalSettings(){
    }

    public LocalSettings(General general, Map map, TwoFields db, TwoFields df, ED ed, PC pc, BPSS bpss, Sound sound) {
        this.general = general;
        this.map = map;
        this.db = db;
        this.df = df;
        this.ed = ed;
        this.pc = pc;
        this.bpss = bpss;
        this.sound = sound;
    }

    public General getGeneral() {
        return general;
    }

    public void setGeneral(General general) {
        this.general = general;
    }

    public Map getMap() {
        return map;
    }

    public void setMap(Map map) {
        this.map = map;
    }

    public TwoFields getDb() {
        return db;
    }

    public void setDb(TwoFields db) {
        this.db = db;
    }

    public TwoFields getDf() {
        return df;
    }

    public void setDf(TwoFields df) {
        this.df = df;
    }

    public ED getEd() {
        return ed;
    }

    public void setEd(ED ed) {
        this.ed = ed;
    }

    public PC getPc() {
        return pc;
    }

    public void setPc(PC pc) {
        this.pc = pc;
    }

    public BPSS getBpss() {
        return bpss;
    }

    public void setBpss(BPSS bpss) {
        this.bpss = bpss;
    }

    public Sound getSound() {
        return sound;
    }

    public void setSound(Sound sound) {
        this.sound = sound;
    }
}
