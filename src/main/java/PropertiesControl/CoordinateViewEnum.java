package PropertiesControl;

public enum CoordinateViewEnum {
    DDDDDDDD(1, "DD.dddddd"),
    DDMMMMMM(2, "DD MM.mmmm"),
    DDMMSSSS(3, "DD MM SS.ss");

    private Integer type;
    private String name;

    CoordinateViewEnum(Integer i, String n) {
        type = i;
        name = n;
    }

    public Integer getCoordinateType() {
        return type;
    }

    public String getCoordinateName() {
        return name;
    }
}