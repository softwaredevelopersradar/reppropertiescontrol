package PropertiesControl;

public class PC {
    Integer type = 1;
    TwoFields modem_4wire = new TwoFields();
    TwoFields radiomodem = new TwoFields();

    public PC(){
    }

    public PC(Integer type, TwoFields modem_4wire, TwoFields radiomodem) {
        this.type = type;
        this.modem_4wire = modem_4wire;
        this.radiomodem = radiomodem;
    }

    public PC(PC pc){
        this.type = pc.type;
        this.modem_4wire = pc.modem_4wire;
        this.radiomodem = pc.radiomodem;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public TwoFields getModem_4wire() {
        return modem_4wire;
    }

    public void setModem_4wire(TwoFields modem_4wire) {
        this.modem_4wire = modem_4wire;
    }

    public TwoFields getRadiomodem() {
        return radiomodem;
    }

    public void setRadiomodem(TwoFields radiomodem) {
        this.radiomodem = radiomodem;
    }
}
