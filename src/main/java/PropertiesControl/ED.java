package PropertiesControl;

public class ED {
    Integer type = 1;
    FourFields modem_4wire = new FourFields();
    FourFields radiomodem = new FourFields();
    FourFields router3G_4G = new FourFields();

    public ED(){
    }

    public ED(Integer type, FourFields modem_4wire, FourFields radiomodem, FourFields router3G_4G){
        this.type = type;
        this.modem_4wire = modem_4wire;
        this.radiomodem = radiomodem;
        this.router3G_4G = router3G_4G;
    }

    public ED(ED ed){
        this.type = ed.type;
        this.modem_4wire = ed.modem_4wire;
        this.radiomodem = ed.radiomodem;
        this.router3G_4G = ed.router3G_4G;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public FourFields getModem_4wire() {
        return modem_4wire;
    }

    public void setModem_4wire(FourFields modem_4wire) {
        this.modem_4wire = modem_4wire;
    }

    public FourFields getRadiomodem() {
        return radiomodem;
    }

    public void setRadiomodem(FourFields radiomodem) {
        this.radiomodem = radiomodem;
    }

    public FourFields getRouter3G_4G() {
        return router3G_4G;
    }

    public void setRouter3G_4G(FourFields router3G_4G) {
        this.router3G_4G = router3G_4G;
    }
}
