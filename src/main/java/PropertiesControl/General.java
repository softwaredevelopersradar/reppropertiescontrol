package PropertiesControl;

public class General {

    Integer AWS = 1;
    AccessEnum Access = AccessEnum.USER;
    LanguageEnum Language = LanguageEnum.RUSSIAN;
    CoordinateViewEnum CoordinateView = CoordinateViewEnum.DDDDDDDD;

    public General() {
    }

    public General(Integer AWS,
                   AccessEnum Access,
                   LanguageEnum Language, CoordinateViewEnum CoordinateView) {
        this.AWS = AWS;
        this.Access = Access;
        this.Language = Language;
        this.CoordinateView = CoordinateView;
    }

    public Integer getAWS() {
        return AWS;
    }

    public void setAWS(Integer AWS) {
        this.AWS = AWS;
    }

    public AccessEnum getAccess() {
        return Access;
    }

    public void setAccess(AccessEnum access) {
        Access = access;
    }

    public LanguageEnum getLanguage() {
        return Language;
    }

    public void setLanguage(LanguageEnum language) {
        Language = language;
    }

    public CoordinateViewEnum getCoordinateView() {
        return CoordinateView;
    }

    public void setCoordinateView(CoordinateViewEnum coordinateView) {
        CoordinateView = coordinateView;
    }
}
