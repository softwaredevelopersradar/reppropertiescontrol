package PropertiesControl;

import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

public class BPSS {
    String Com_Port = "";
    Integer Speed = 2400;

    public BPSS() {
    }

    public BPSS (Integer Speed){
        this.Speed = Speed;
    }

    public BPSS (String Com_Port, Integer Speed){
        this.Com_Port = Com_Port;
        this.Speed = Speed;
    }

    public String getCom_Port() {
        return Com_Port;
    }

    public void setCom_Port(String com_Port) {
        Com_Port = com_Port;
    }

    public Integer getSpeed() {
        return Speed;
    }

    public void setSpeed(Integer speed) {
        Speed = speed;
    }
}
