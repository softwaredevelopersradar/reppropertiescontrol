package PropertiesControl;

import KvetkaModels.GlobalPropertiesModel;

public interface IPropertiesEvent {

    void OnLocalProperties(LocalSettings localSettings);
    void OnGlobalProperties(GlobalPropertiesModel globalPropertiesModel);
}
