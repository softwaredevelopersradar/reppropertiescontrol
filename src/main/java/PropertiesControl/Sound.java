package PropertiesControl;

import javafx.concurrent.Task;

public class Sound {
    Integer Sound = 8000;
    Double AGLLevel = 0.0;
    Double MGCFactor = 0.0;
    Integer type = 1;
//    String TADevice = "";
//    String ListeningDevice = "";

    public Sound() {
    }

    public Sound (Integer Sound, Double AGLLevel, Double MGCFactor
//            , String TADevice, String ListeningDevice
                  , Integer type
    ){
        this.Sound = Sound;
        this.AGLLevel = AGLLevel;
        this.MGCFactor = MGCFactor;
//        this.TADevice = TADevice;
//        this.ListeningDevice = ListeningDevice;
        this.type = type;
    }

    public Integer getSound() {
        return Sound;
    }

    public void setSound(Integer sound) {
        Sound = sound;
    }

    public Double getAGLLevel() {
        return AGLLevel;
    }

    public void setAGLLevel(Double AGLLevel) {
        this.AGLLevel = AGLLevel;
    }

    public Double getMGCFactor() {
        return MGCFactor;
    }

    public void setMGCFactor(Double MGCFactor) {
        this.MGCFactor = MGCFactor;
    }

    //    public String getTADevice() {
//        return TADevice;
//    }
//
//    public void setTADevice(String TADevice) {
//        this.TADevice = TADevice;
//    }
//
//    public String getListeningDevice() {
//        return ListeningDevice;
//    }
//
//    public void setListeningDevice(String listeningDevice) {
//        ListeningDevice = listeningDevice;
//    }


    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }
}