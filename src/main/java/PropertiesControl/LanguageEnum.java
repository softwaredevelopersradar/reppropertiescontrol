package PropertiesControl;

public enum  LanguageEnum {
    RUSSIAN(1, "Русский"),
    ENGLISH(2, "Английский");

    private Integer type;
    private String name;

    LanguageEnum(Integer a, String n) {
        type = a;
        name = n;
    }

    public Integer getLanguageType() {
        return type;
    }

    public String getLanguageName() {
        return name;
    }
}
