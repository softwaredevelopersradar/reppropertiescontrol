package PropertiesControl;

public enum TypeCntEnum {
    Modem_4wire(1, "Модем 2, 4-проводный"),
    Radiomodem(2, "Радиомодем"),
    Router_3G_4G(3, "3G_4G роутер");

    private Integer type;
    private String name;

    TypeCntEnum(Integer i, String n) {
        type = i;
        name = n;
    }

    public Integer getCntType() {
        return type;
    }

    public String getCntName(){
        return name;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public void setName(String name) {
        this.name = name;
    }
}
