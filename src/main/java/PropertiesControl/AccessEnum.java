package PropertiesControl;

public enum AccessEnum {
    USER( 1, "Пользователь"),
    ADMIN( 2, "Админ");

    private Integer type;
    private String name;

    AccessEnum(Integer a, String n) {
        type = a;
        name = n;
    }

    public Integer getAccessType() {
        return type;
    }

    public String getAccessName() {
        return name;
    }
}
