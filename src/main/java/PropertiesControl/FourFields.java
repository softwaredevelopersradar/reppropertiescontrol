package PropertiesControl;

public class FourFields {

    TwoFields local = new TwoFields();
    TwoFields remote = new TwoFields();

    public FourFields(){
    }

    public FourFields(TwoFields local, TwoFields remote){
        this.local = new TwoFields(local);
        this.remote = new TwoFields(remote);
    }

    public TwoFields getLocal() {
        return local;
    }

    public void setLocal(TwoFields local) {
        this.local = local;
    }

    public TwoFields getRemote() {
        return remote;
    }

    public void setRemote(TwoFields remote) {
        this.remote = remote;
    }
}
