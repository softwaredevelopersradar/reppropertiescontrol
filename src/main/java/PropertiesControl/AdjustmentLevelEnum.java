package PropertiesControl;

public enum AdjustmentLevelEnum {
    AUTO( 1, "Автоматический"),
    MANUAL( 2, "Ручной");

    private Integer type;
    private String name;

    AdjustmentLevelEnum(Integer a, String n) {
        type = a;
        name = n;
    }

    public Integer getAccessType() {
        return type;
    }

    public String getAccessName() {
        return name;
    }
}

