package PropertiesControl;

import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

public class TwoFields {
    String IPAddress = "127.0.0.1";
    Integer Port = 80;

    public TwoFields() {
    }

    public TwoFields(String IPAddress, Integer Port) {
        this.IPAddress = IPAddress;
        this.Port = Port;
    }

    public TwoFields(TwoFields twoFields) {
        this.IPAddress = twoFields.IPAddress;
        this.Port = twoFields.Port;
    }

    public String getIPAddress() {
        return IPAddress;
    }

    public void setIPAddress(String IPAddress) {
        this.IPAddress = IPAddress;
    }

    public Integer getPort() {
        return Port;
    }

    public void setPort(Integer port) {
        Port = port;
    }
}
