package PropertiesControl;

public class Map {
    String MapPath = "";
    String MatrixPath = "";
    String MapDFPath = "";
    String MatrixDFPath = "";
    int DistanceBearingLine = 10;

    public Map() {
    }

    public Map(String MapPath, String MatrixPath, String MapDFPath, String MatrixDFPath, int DistanceBearingLine) {
        this.MapPath = MapPath;
        this.MatrixPath = MatrixPath;
        this.MapDFPath = MapDFPath;
        this.MatrixDFPath = MatrixDFPath;
        this.DistanceBearingLine = DistanceBearingLine;
    }

    public String getMapPath() {
        return MapPath;
    }

    public void setMapPath(String mapPath) {
        MapPath = mapPath;
    }

    public String getMatrixPath() {
        return MatrixPath;
    }

    public void setMatrixPath(String matrixPath) {
        MatrixPath = matrixPath;
    }

    public String getMapDFPath() {
        return MapDFPath;
    }

    public void setMapDFPath(String mapDFPath) {
        MapDFPath = mapDFPath;
    }

    public String getMatrixDFPath() {
        return MatrixDFPath;
    }

    public void setMatrixDFPath(String matrixDFPath) {
        MatrixDFPath = matrixDFPath;
    }

    public int getDistanceBearingLine() {
        return DistanceBearingLine;
    }

    public void setDistanceBearingLine(int distanceBearingLine) {
        DistanceBearingLine = distanceBearingLine;
    }
}
