package PropertiesControl;

import KvetkaModels.Coord;
import KvetkaModels.FftResolution;
import KvetkaModels.GlobalPropertiesModel;
import KvetkaModels.PowerPercent;
import com.fazecast.jSerialComm.SerialPort;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;

import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Mixer;
import java.io.IOException;
import java.net.URL;
import java.util.*;
import java.util.List;

public class PropertiesControl extends AnchorPane implements Initializable, IAdminWindowEvent {

    public PropertiesControl() {

        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("Settings.fxml"));
        fxmlLoader.setRoot(this);
        fxmlLoader.setController(this);

        try {
            fxmlLoader.load();
        } catch (IOException exception) {

            throw new RuntimeException(exception);
        }
    }

    @FXML
    VBox vBox;
    @FXML
    TextField MapPath, MatrixPath, MapDFPath, MatrixDFPath, DistanceBearingLine;
    @FXML
    ComboBox<String> AWSBox;
    @FXML
    ComboBox<String> AccessBox;
    @FXML
    ComboBox<String> LanguageBox;
    @FXML
    ComboBox<String> CoordinateViewBox;
    @FXML
    ComboBox<String> EDTypeBox;
    @FXML
    ComboBox<String> PCTypeBox;
    @FXML
    ComboBox<String> ComBox;
    @FXML
    ComboBox<String> SpeedBox;
    @FXML
    ComboBox<Integer> SoundBox;
    @FXML
    ComboBox<String> FFTResolutionBox, PowerBox;
    @FXML
    ComboBox<String> TADeviceBox, ListeningDeviceBox;
    @FXML
    ComboBox<String> AdjustmentLevelBox;
    @FXML
    TextField JammingTime, NoPA, NoBA, CourseAngle, AdaptiveThresholdFhss, LSRLTime, Priority, Threshold, RadiationTime, FHSSRadiationTime, ChannelLitter, IRILitter, SearchSector, SearchTime, Latitude, Longtitude, Altitude, PATime;
    @FXML
    TextField DBIPAddress, DFIPAddress, DBPort, DFPort, EDLocalIPModem_4wire, EDLocalPortModem_4wire, EDRemoteIPModem_4wire, EDRemotePortModem_4wire, EDLocalIPRadiomodem, EDLocalPortRadiomodem, EDRemoteIPRadiomodem, EDRemotePortRadiomodem, EDLocalIP3G_4G, EDLocalPort3G_4G, EDRemoteIP3G_4G, EDRemotePort3G_4G, PCRemoteIPRadiomodem, PCRemotePortRadiomodem, PCRemoteIPModem_4wire, PCRemotePortModem_4wire, AGLLevel, MGCFactor;
    @FXML
    TitledPane DBTitledPane, DFTitledPane, EDTitledPane, PCTitledPane, BPSSTitledPane;
    @FXML
    TitledPane GNSSTitledPane, RFAutotuningTitledPane;
    @FXML
    GridPane PCGridPane;
    @FXML
    GridPane SoundGrid;
    @FXML
    CheckBox TestCheck;
    @FXML
    Label LabelModem_4wire, LabelRadiomodem, LabelPCRemoteIPModem_4wire, LabelPCRemotePortModem_4wire, LabelPCRemoteIPRadiomodem, LabelPCRemotePortRadiomodem;
    @FXML
    Label TALabel, ListeningLabel, AGLLevelLabel, MGCFactorLabel;

    private PropertyControllerForAdmin propertyControllerForAdmin;

    public GlobalPropertiesModel globalPropertiesModel = new GlobalPropertiesModel();
    public Coord coord = new Coord();
    public LocalSettings localSettings = new LocalSettings();
    public ObservableList<Integer> SoundList = FXCollections.observableArrayList(8000);
    public ObservableList<String> TADeviceList = FXCollections.observableArrayList();
    public ObservableList<String> ListeningDeviceList = FXCollections.observableArrayList();

    public boolean flag1;
    public boolean flag2;
    public boolean flag3;

    String selectedValue;
    String selectedValue1;
    String selectedValue2;
    String selectedValue3;
    String selectedValue4;
    String selectedValue5;
    String selectedValue6;
    String selectedValue7;
    Integer selectedValue8;
    String selectedValue9;
    String selectedValue10;
    String selectedValue11;
    String selectedValue20;
    String selectedValue21;

    public List<IPropertiesEvent> listeners = new ArrayList<>();

    public void addListener(IPropertiesEvent toAdd) {
        listeners.add(toAdd);
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {

        //LOCAL SETTINGS

        AWSBox.getItems().setAll(
                "1",
                "2",
                "3",
                "4",
                "5",
                "6",
                "7",
                "8",
                "9",
                "10"
        );

        AccessBox.getItems().setAll(
                AccessEnum.USER.getAccessName(),
                AccessEnum.ADMIN.getAccessName()
        );
        AccessBox.setValue(AccessEnum.USER.getAccessName());
        AdminWindowAccessDenied();

        LanguageBox.getItems().setAll(
                "Русский",
                "Английский"
        );
        CoordinateViewBox.getItems().setAll(
                CoordinateViewEnum.DDDDDDDD.getCoordinateName(),
                CoordinateViewEnum.DDMMMMMM.getCoordinateName(),
                CoordinateViewEnum.DDMMSSSS.getCoordinateName()
        );
        EDTypeBox.getItems().setAll(
                TypeCntEnum.Modem_4wire.getCntName(),
                TypeCntEnum.Radiomodem.getCntName(),
                TypeCntEnum.Router_3G_4G.getCntName()
        );
        PCTypeBox.getItems().setAll(
                TypeCntEnum.Modem_4wire.getCntName(),
                TypeCntEnum.Radiomodem.getCntName()
        );
        if (SerialPort.getCommPorts().length != 0) {
            ComBox.getItems().setAll(getListPortNames());
        } else ComBox.getItems().setAll("");
        SpeedBox.getItems().setAll(
                "2400",
                "4800",
                "9600",
                "19200",
                "38400",
                "57600",
                "115200"
        );
//        captureAudio();
//        if (AudioSystem.getMixerInfo().length != 0)
//            TADeviceBox.getItems().setAll(TADeviceList);
//        else TADeviceBox.getItems().setAll("");
//        if (AudioSystem.getMixerInfo().length != 0)
//            ListeningDeviceBox.getItems().setAll(ListeningDeviceList);
//        else ListeningDeviceBox.getItems().setAll("");

        DBIPAddress.setOnMousePressed(mouseEvent -> DBIPAddress.setStyle("-fx-border-color: grey"));
        DBPort.setOnMousePressed(mouseEvent -> DBPort.setStyle("-fx-border-color: grey"));
        DFIPAddress.setOnMousePressed(mouseEvent -> DFIPAddress.setStyle("-fx-border-color: grey"));
        DFPort.setOnMousePressed(mouseEvent -> DFPort.setStyle("-fx-border-color: grey"));
        EDLocalIPModem_4wire.setOnMousePressed(mouseEvent -> EDLocalIPModem_4wire.setStyle("-fx-border-color: grey"));
        EDLocalPortModem_4wire.setOnMousePressed(mouseEvent -> EDLocalPortModem_4wire.setStyle("-fx-border-color: grey"));
        EDRemoteIPModem_4wire.setOnMousePressed(mouseEvent -> EDRemoteIPModem_4wire.setStyle("-fx-border-color: grey"));
        EDRemotePortModem_4wire.setOnMousePressed(mouseEvent -> EDRemotePortModem_4wire.setStyle("-fx-border-color: grey"));
        EDLocalIPRadiomodem.setOnMousePressed(mouseEvent -> EDLocalIPRadiomodem.setStyle("-fx-border-color: grey"));
        EDLocalPortRadiomodem.setOnMousePressed(mouseEvent -> EDLocalPortRadiomodem.setStyle("-fx-border-color: grey"));
        EDRemoteIPRadiomodem.setOnMousePressed(mouseEvent -> EDRemoteIPRadiomodem.setStyle("-fx-border-color: grey"));
        EDRemotePortRadiomodem.setOnMousePressed(mouseEvent -> EDRemotePortRadiomodem.setStyle("-fx-border-color: grey"));
        EDLocalIP3G_4G.setOnMousePressed(mouseEvent -> EDLocalIP3G_4G.setStyle("-fx-border-color: grey"));
        EDLocalPort3G_4G.setOnMousePressed(mouseEvent -> EDLocalPort3G_4G.setStyle("-fx-border-color: grey"));
        EDRemoteIP3G_4G.setOnMousePressed(mouseEvent -> EDRemoteIP3G_4G.setStyle("-fx-border-color: grey"));
        EDRemotePort3G_4G.setOnMousePressed(mouseEvent -> EDRemotePort3G_4G.setStyle("-fx-border-color: grey"));
        PCRemoteIPModem_4wire.setOnMousePressed(mouseEvent -> PCRemoteIPModem_4wire.setStyle("-fx-border-color: grey"));
        PCRemotePortModem_4wire.setOnMousePressed(mouseEvent -> PCRemotePortModem_4wire.setStyle("-fx-border-color: grey"));
        PCRemoteIPRadiomodem.setOnMousePressed(mouseEvent -> PCRemoteIPRadiomodem.setStyle("-fx-border-color: grey"));
        PCRemotePortRadiomodem.setOnMousePressed(mouseEvent -> PCRemotePortRadiomodem.setStyle("-fx-border-color: grey"));
        AGLLevel.setOnMousePressed(mouseEvent -> AGLLevel.setStyle("-fx-border-color: grey"));
        MGCFactor.setOnMousePressed(mouseEvent -> MGCFactor.setStyle("-fx-border-color: grey"));

        //GLOBAL SETTINGS

        JammingTime.setOnMousePressed(mouseEvent -> JammingTime.setStyle("-fx-border-color: grey"));
        NoPA.setOnMousePressed(mouseEvent -> NoPA.setStyle("-fx-border-color: grey"));
        NoBA.setOnMousePressed(mouseEvent -> NoBA.setStyle("-fx-border-color: grey"));
        CourseAngle.setOnMousePressed(mouseEvent -> CourseAngle.setStyle("-fx-border-color: grey"));
        AdaptiveThresholdFhss.setOnMousePressed(mouseEvent -> AdaptiveThresholdFhss.setStyle("-fx-border-color: grey"));
        ChannelLitter.setOnMousePressed(mouseEvent -> ChannelLitter.setStyle("-fx-border-color: grey"));
        IRILitter.setOnMousePressed(mouseEvent -> IRILitter.setStyle("-fx-border-color: grey"));
        LSRLTime.setOnMousePressed(mouseEvent -> LSRLTime.setStyle("-fx-border-color: grey"));
        Priority.setOnMousePressed(mouseEvent -> Priority.setStyle("-fx-border-color: grey"));
        Threshold.setOnMousePressed(mouseEvent -> Threshold.setStyle("-fx-border-color: grey"));
        RadiationTime.setOnMousePressed(mouseEvent -> RadiationTime.setStyle("-fx-border-color: grey"));
        FHSSRadiationTime.setOnMousePressed(mouseEvent -> FHSSRadiationTime.setStyle("-fx-border-color: grey"));
        PATime.setOnMousePressed(mouseEvent -> PATime.setStyle("-fx-border-color: grey"));

//        SearchSector.setOnMousePressed(mouseEvent -> SearchSector.setStyle("-fx-border-color: grey"));
//        SearchTime.setOnMousePressed(mouseEvent -> SearchTime.setStyle("-fx-border-color: grey"));
//        Latitude.setOnMousePressed(mouseEvent -> Latitude.setStyle("-fx-border-color: grey"));
//        Longtitude.setOnMousePressed(mouseEvent -> Longtitude.setStyle("-fx-border-color: grey"));
//        Altitude.setOnMousePressed(mouseEvent -> Altitude.setStyle("-fx-border-color: grey"));

        FFTResolutionBox.getItems().setAll(
                FftResolution.N16384.getFftResolutionName(),
                FftResolution.N8192.getFftResolutionName(),
                FftResolution.N4096.getFftResolutionName()
        );
        PowerBox.getItems().setAll(
                PowerPercent.P25.getPowerPercentName(),
                PowerPercent.P50.getPowerPercentName(),
                PowerPercent.P100.getPowerPercentName()
        );

        GNSSTitledPane.setMinHeight(0);
        GNSSTitledPane.setMaxHeight(0);
        RFAutotuningTitledPane.setMinHeight(0);
        RFAutotuningTitledPane.setMaxHeight(0);

        AdjustmentLevelBox.getItems().setAll(
                AdjustmentLevelEnum.AUTO.getAccessName(),
                AdjustmentLevelEnum.MANUAL.getAccessName()
        );

    }

    public void checkTypes(Integer Type_OD, Integer Type_PC, Integer Type_Sound){
        if (Type_OD.equals(TypeCntEnum.Modem_4wire.getCntType())) {
            EDLocalIPModem_4wire.setDisable(false);
            EDLocalPortModem_4wire.setDisable(false);
            EDRemoteIPModem_4wire.setDisable(false);
            EDRemotePortModem_4wire.setDisable(false);

            EDLocalIPRadiomodem.setDisable(true);
            EDLocalPortRadiomodem.setDisable(true);
            EDRemoteIPRadiomodem.setDisable(true);
            EDRemotePortRadiomodem.setDisable(true);

            EDLocalIP3G_4G.setDisable(true);
            EDLocalPort3G_4G.setDisable(true);
            EDRemoteIP3G_4G.setDisable(true);
            EDRemotePort3G_4G.setDisable(true);
        } else if (Type_OD.equals(TypeCntEnum.Radiomodem.getCntType())) {
            EDLocalIPModem_4wire.setDisable(true);
            EDLocalPortModem_4wire.setDisable(true);
            EDRemoteIPModem_4wire.setDisable(true);
            EDRemotePortModem_4wire.setDisable(true);

            EDLocalIPRadiomodem.setDisable(false);
            EDLocalPortRadiomodem.setDisable(false);
            EDRemoteIPRadiomodem.setDisable(false);
            EDRemotePortRadiomodem.setDisable(false);

            EDLocalIP3G_4G.setDisable(true);
            EDLocalPort3G_4G.setDisable(true);
            EDRemoteIP3G_4G.setDisable(true);
            EDRemotePort3G_4G.setDisable(true);
        } else if (Type_OD.equals(TypeCntEnum.Router_3G_4G.getCntType())) {
            EDLocalIPModem_4wire.setDisable(true);
            EDLocalPortModem_4wire.setDisable(true);
            EDRemoteIPModem_4wire.setDisable(true);
            EDRemotePortModem_4wire.setDisable(true);

            EDLocalIPRadiomodem.setDisable(true);
            EDLocalPortRadiomodem.setDisable(true);
            EDRemoteIPRadiomodem.setDisable(true);
            EDRemotePortRadiomodem.setDisable(true);

            EDLocalIP3G_4G.setDisable(false);
            EDLocalPort3G_4G.setDisable(false);
            EDRemoteIP3G_4G.setDisable(false);
            EDRemotePort3G_4G.setDisable(false);
        }
        if (Type_PC.equals(TypeCntEnum.Modem_4wire.getCntType())) {
            PCRemoteIPModem_4wire.setDisable(false);
            PCRemotePortModem_4wire.setDisable(false);

            PCRemoteIPRadiomodem.setDisable(true);
            PCRemotePortRadiomodem.setDisable(true);
        } else if (Type_PC.equals(TypeCntEnum.Radiomodem.getCntType())) {
            PCRemoteIPModem_4wire.setDisable(true);
            PCRemotePortModem_4wire.setDisable(true);

            PCRemoteIPRadiomodem.setDisable(false);
            PCRemotePortRadiomodem.setDisable(false);
        }

        if (Type_Sound.equals(AdjustmentLevelEnum.AUTO.getAccessType())){
            MGCFactorLabel.setVisible(false);
            MGCFactor.setVisible(false);
            AGLLevel.setVisible(true);
            AGLLevelLabel.setVisible(true);
            SoundGrid.getRowConstraints().get(2).setMaxHeight(0);
            SoundGrid.getRowConstraints().get(2).setMinHeight(0);
            SoundGrid.getRowConstraints().get(3).setMaxHeight(30);
            SoundGrid.getRowConstraints().get(3).setMinHeight(30);
        } else if (Type_Sound.equals(AdjustmentLevelEnum.MANUAL.getAccessType())){
            MGCFactorLabel.setVisible(true);
            MGCFactor.setVisible(true);
            AGLLevel.setVisible(false);
            AGLLevelLabel.setVisible(false);
            SoundGrid.getRowConstraints().get(2).setMaxHeight(30);
            SoundGrid.getRowConstraints().get(2).setMinHeight(30);
            SoundGrid.getRowConstraints().get(3).setMaxHeight(0);
            SoundGrid.getRowConstraints().get(3).setMinHeight(0);
        }
    }

    @FXML
    public void access(ActionEvent actionEvent) {

        selectedValue = AccessBox.getSelectionModel().getSelectedItem();

        if (selectedValue.equals(AccessEnum.USER.getAccessName())) {
            AdminWindowAccessDenied();
        } else if (selectedValue.equals(AccessEnum.ADMIN.getAccessName())) {
            propertyControllerForAdmin = new PropertyControllerForAdmin();
            propertyControllerForAdmin.addListenerProperty(this);
        }
    }


    @Override
    public void AdminWindowAccessGranted() {
        DBTitledPane.setMinHeight(USE_COMPUTED_SIZE);
        DBTitledPane.setMaxHeight(USE_COMPUTED_SIZE);

        DFTitledPane.setMinHeight(USE_COMPUTED_SIZE);
        DFTitledPane.setMaxHeight(USE_COMPUTED_SIZE);

        EDTitledPane.setMinHeight(USE_COMPUTED_SIZE);
        EDTitledPane.setMaxHeight(USE_COMPUTED_SIZE);

        PCGridPane.setMaxHeight(USE_COMPUTED_SIZE);
        PCGridPane.setMinHeight(USE_COMPUTED_SIZE);
        PCGridPane.getRowConstraints().get(1).setMinHeight(30);
        PCGridPane.getRowConstraints().get(1).setMaxHeight(30);
        PCGridPane.getRowConstraints().get(2).setMinHeight(30);
        PCGridPane.getRowConstraints().get(2).setMaxHeight(30);
        PCGridPane.getRowConstraints().get(3).setMinHeight(30);
        PCGridPane.getRowConstraints().get(3).setMaxHeight(30);
        PCGridPane.getRowConstraints().get(4).setMinHeight(30);
        PCGridPane.getRowConstraints().get(4).setMaxHeight(30);
        PCGridPane.getRowConstraints().get(5).setMinHeight(30);
        PCGridPane.getRowConstraints().get(5).setMaxHeight(30);
        LabelModem_4wire.setVisible(true);
        LabelPCRemoteIPModem_4wire.setVisible(true);
        LabelPCRemotePortModem_4wire.setVisible(true);
        LabelRadiomodem.setVisible(true);
        LabelPCRemoteIPRadiomodem.setVisible(true);
        LabelPCRemotePortRadiomodem.setVisible(true);
        PCRemoteIPModem_4wire.setVisible(true);
        PCRemotePortModem_4wire.setVisible(true);
        PCRemoteIPRadiomodem.setVisible(true);
        PCRemotePortRadiomodem.setVisible(true);

        BPSSTitledPane.setMinHeight(USE_COMPUTED_SIZE);
        BPSSTitledPane.setMaxHeight(USE_COMPUTED_SIZE);
    }

    @Override
    public void AdminWindowAccessDenied() {
        AccessBox.setValue(AccessEnum.USER.getAccessName());

        DBTitledPane.setMinHeight(0);
        DBTitledPane.setMaxHeight(0);

        DFTitledPane.setMinHeight(0);
        DFTitledPane.setMaxHeight(0);

        EDTitledPane.setMinHeight(0);
        EDTitledPane.setMaxHeight(0);

        PCGridPane.setMaxHeight(50);
        PCGridPane.setMinHeight(50);
        PCGridPane.getRowConstraints().get(1).setMinHeight(0);
        PCGridPane.getRowConstraints().get(1).setMaxHeight(0);
        PCGridPane.getRowConstraints().get(2).setMinHeight(0);
        PCGridPane.getRowConstraints().get(2).setMaxHeight(0);
        PCGridPane.getRowConstraints().get(3).setMinHeight(0);
        PCGridPane.getRowConstraints().get(3).setMaxHeight(0);
        PCGridPane.getRowConstraints().get(4).setMinHeight(0);
        PCGridPane.getRowConstraints().get(4).setMaxHeight(0);
        PCGridPane.getRowConstraints().get(5).setMinHeight(0);
        PCGridPane.getRowConstraints().get(5).setMaxHeight(0);
        LabelModem_4wire.setVisible(false);
        LabelPCRemoteIPModem_4wire.setVisible(false);
        LabelPCRemotePortModem_4wire.setVisible(false);
        LabelRadiomodem.setVisible(false);
        LabelPCRemoteIPRadiomodem.setVisible(false);
        LabelPCRemotePortRadiomodem.setVisible(false);
        PCRemoteIPModem_4wire.setVisible(false);
        PCRemotePortModem_4wire.setVisible(false);
        PCRemoteIPRadiomodem.setVisible(false);
        PCRemotePortRadiomodem.setVisible(false);

        BPSSTitledPane.setMinHeight(0);
        BPSSTitledPane.setMaxHeight(0);

        SoundGrid.getRowConstraints().get(4).setMinHeight(0);
        SoundGrid.getRowConstraints().get(4).setMaxHeight(0);
        SoundGrid.getRowConstraints().get(5).setMinHeight(0);
        SoundGrid.getRowConstraints().get(5).setMaxHeight(0);
        TALabel.setVisible(false);
        ListeningLabel.setVisible(false);
        TADeviceBox.setVisible(false);
        ListeningDeviceBox.setVisible(false);
        SoundGrid.setMinHeight(110);
        SoundGrid.setMaxHeight(110);
    }

//    public void CheckAdminField(LocalSettings localSettings) throws IOException {
//        //get local settings
//        if (localSettings.getGeneral().getAccess().getAccessType().equals(AccessEnum.USER.getAccessType())) {
//            AdminWindowAccessDenied();
//        } else if (localSettings.getGeneral().getAccess().getAccessType().equals(AccessEnum.ADMIN.getAccessType())) {
//            PropertyControllerForAdmin propertyControllerForAdmin = new PropertyControllerForAdmin();
//            propertyControllerForAdmin.addListenerProperty(this);
//        }
//    }

    @FXML
    public void ButtonYes_Click(ActionEvent event) {
        OnUpdateLocal();
        OnUpdateGlobal();
    }

    public void UpdGlobal(GlobalPropertiesModel model) {
        JammingTime.setText(String.valueOf(model.getTimeJamming()));
        NoPA.setText(String.valueOf(model.getNumberOfPhasesAveragings()));
        NoBA.setText(String.valueOf(model.getNumberOfBearingAveragings()));
        CourseAngle.setText(String.valueOf(model.getCourseAngle()));
        AdaptiveThresholdFhss.setText(String.valueOf(model.getAdaptiveThresholdFhss()));
        ChannelLitter.setText(String.valueOf(model.getNumberOfChannelsInLetter()));
        IRILitter.setText(String.valueOf(model.getNumberOfResInLetter()));
        LSRLTime.setText(String.valueOf(model.getLongWorkingSignalDurationMs()));
        Priority.setText(String.valueOf(model.getPriority()));
        Threshold.setText(String.valueOf(model.getThreshold()));
        RadiationTime.setText(String.valueOf(model.getTimeRadiationFF()));
        TestCheck.setSelected(model.getIsTest());
        if (globalPropertiesModel.getFhssFftResolution().getFftResolutionType() == 3) {
            FFTResolutionBox.setValue(FftResolution.N16384.getFftResolutionName());
        } else if (globalPropertiesModel.getFhssFftResolution().getFftResolutionType() == 4) {
            FFTResolutionBox.setValue(FftResolution.N8192.getFftResolutionName());
        } else if (globalPropertiesModel.getFhssFftResolution().getFftResolutionType() == 5) {
            FFTResolutionBox.setValue(FftResolution.N4096.getFftResolutionName());
        }
        FHSSRadiationTime.setText(String.valueOf(model.getTimeRadiationFHSS()));
        PATime.setText(String.valueOf(model.getJsgAmPollInterval()));
        if (globalPropertiesModel.getJsgAmPower().getPowerPercentType() == 0) {
            PowerBox.setValue(PowerPercent.P100.getPowerPercentName());
        } else if (globalPropertiesModel.getJsgAmPower().getPowerPercentType() == 1) {
            PowerBox.setValue(PowerPercent.P50.getPowerPercentName());
        } else if (globalPropertiesModel.getJsgAmPower().getPowerPercentType() == 2) {
            PowerBox.setValue(PowerPercent.P25.getPowerPercentName());
        }

//        SearchSector.setText(String.valueOf(model.getSearchSector()));
//        SearchTime.setText(String.valueOf(model.getSearchTime()));
//        Latitude.setText(String.valueOf(model.getGnss().latitude));
//        Longtitude.setText(String.valueOf(model.getGnss().longitude));
//        Altitude.setText(String.valueOf(model.getGnss().altitude));
    }

    public void UpdLocal(LocalSettings model) {
        AWSBox.setValue(String.valueOf(model.getGeneral().getAWS()));

        if (model.getGeneral().getAccess().getAccessType().equals(AccessEnum.USER.getAccessType()))
            AccessBox.setValue(AccessEnum.USER.getAccessName());
        else if (model.getGeneral().getAccess().getAccessType().equals(AccessEnum.ADMIN.getAccessType())) {
            AccessBox.setValue(AccessEnum.ADMIN.getAccessName());
        }

        if (model.getGeneral().getLanguage().getLanguageType().equals(LanguageEnum.RUSSIAN.getLanguageType())) {
            LanguageBox.setValue(LanguageEnum.RUSSIAN.getLanguageName());
        } else if (model.getGeneral().getLanguage().getLanguageType().equals(LanguageEnum.ENGLISH.getLanguageType())) {
            LanguageBox.setValue(LanguageEnum.ENGLISH.getLanguageName());
        }

        if (model.getGeneral().getCoordinateView().getCoordinateType().equals(CoordinateViewEnum.DDDDDDDD.getCoordinateType()))
            CoordinateViewBox.setValue(CoordinateViewEnum.DDDDDDDD.getCoordinateName());
        else if (model.getGeneral().getCoordinateView().getCoordinateType().equals(CoordinateViewEnum.DDMMMMMM.getCoordinateType())) {
            CoordinateViewBox.setValue(CoordinateViewEnum.DDMMMMMM.getCoordinateName());
        } else if (model.getGeneral().getCoordinateView().getCoordinateType().equals(CoordinateViewEnum.DDMMSSSS.getCoordinateType())) {
            CoordinateViewBox.setValue(CoordinateViewEnum.DDMMSSSS.getCoordinateName());
        }

        MapPath.setText(model.getMap().getMapPath());
        MatrixPath.setText(model.getMap().getMatrixPath());
        MapDFPath.setText(model.getMap().getMapDFPath());
        MatrixDFPath.setText(model.getMap().getMatrixDFPath());
        DistanceBearingLine.setText(String.valueOf(model.getMap().getDistanceBearingLine()));

        DBIPAddress.setText(model.getDb().getIPAddress());
        DBPort.setText((String.valueOf(model.getDb().getPort())));

        DFIPAddress.setText(model.getDf().getIPAddress());
        DFPort.setText(String.valueOf(model.getDf().getPort()));

        if (localSettings.getEd().getType() == 1) {
            EDTypeBox.setValue(TypeCntEnum.Modem_4wire.getCntName());
        } else if (localSettings.getEd().getType() == 2) {
            EDTypeBox.setValue(TypeCntEnum.Radiomodem.getCntName());
        } else if (localSettings.getEd().getType() == 3) {
            EDTypeBox.setValue(TypeCntEnum.Router_3G_4G.getCntName());
        }

        EDLocalIPModem_4wire.setText(model.getEd().getModem_4wire().getLocal().getIPAddress());
        EDLocalPortModem_4wire.setText(String.valueOf(model.getEd().getModem_4wire().getLocal().getPort()));
        EDRemoteIPModem_4wire.setText(model.getEd().getModem_4wire().getRemote().getIPAddress());
        EDRemotePortModem_4wire.setText(String.valueOf(model.getEd().getModem_4wire().getRemote().getPort()));

        EDLocalIPRadiomodem.setText(model.getEd().getRadiomodem().getLocal().getIPAddress());
        EDLocalPortRadiomodem.setText(String.valueOf(model.getEd().getRadiomodem().getLocal().getPort()));
        EDRemoteIPRadiomodem.setText(model.getEd().getRadiomodem().getRemote().getIPAddress());
        EDRemotePortRadiomodem.setText(String.valueOf(model.getEd().getRadiomodem().getRemote().getPort()));

        EDLocalIP3G_4G.setText(model.getEd().getRouter3G_4G().getLocal().getIPAddress());
        EDLocalPort3G_4G.setText(String.valueOf(model.getEd().getRouter3G_4G().getLocal().getPort()));
        EDRemoteIP3G_4G.setText(model.getEd().getRouter3G_4G().getRemote().getIPAddress());
        EDRemotePort3G_4G.setText(String.valueOf(model.getEd().getRouter3G_4G().getRemote().getPort()));

        if (localSettings.getPc().getType() == 1) {
            PCTypeBox.setValue(TypeCntEnum.Modem_4wire.getCntName());
        } else if (localSettings.getPc().getType() == 2) {
            PCTypeBox.setValue(TypeCntEnum.Radiomodem.getCntName());
        }

        PCRemoteIPModem_4wire.setText(model.getPc().getModem_4wire().getIPAddress());
        PCRemotePortModem_4wire.setText(String.valueOf(model.getPc().getModem_4wire().getPort()));

        PCRemoteIPRadiomodem.setText(model.getPc().getRadiomodem().getIPAddress());
        PCRemotePortRadiomodem.setText(String.valueOf(model.getPc().getRadiomodem().getPort()));

        if (SerialPort.getCommPorts().length != 0) {
            ComBox.setValue(localSettings.getBpss().getCom_Port());
        }
        SpeedBox.setValue(
                String.valueOf(model.getBpss().getSpeed())
        );
        SoundBox.setValue(
                model.getSound().getSound()
        );
        if (localSettings.getSound().getType() == 1) {
            AdjustmentLevelBox.setValue(AdjustmentLevelEnum.AUTO.getAccessName());
        } else if (localSettings.getSound().getType() == 2) {
            AdjustmentLevelBox.setValue(AdjustmentLevelEnum.MANUAL.getAccessName());
        }
        AGLLevel.setText(String.valueOf(model.getSound().getAGLLevel()));
        MGCFactor.setText(String.valueOf(model.getSound().getMGCFactor()));
//        TADeviceBox.setValue(model.getSound().getTADevice());
//        ListeningDeviceBox.setValue(model.getSound().getListeningDevice());
    }

    public void OnUpdateLocalGeneral() {
        selectedValue = AccessBox.getSelectionModel().getSelectedItem();
        selectedValue1 = AWSBox.getSelectionModel().getSelectedItem();
        selectedValue2 = LanguageBox.getSelectionModel().getSelectedItem();
        selectedValue3 = CoordinateViewBox.getSelectionModel().getSelectedItem();

        switch (selectedValue) {
            case "Пользователь" -> {
                if (!selectedValue.equals(localSettings.getGeneral().getAccess().getAccessName())) {
                    localSettings.getGeneral().setAccess(AccessEnum.USER);
                    flag3 = true;
                }
            }
            case "Админ" -> {
                if (!selectedValue.equals(localSettings.getGeneral().getAccess().getAccessName())) {
                    localSettings.getGeneral().setAccess(AccessEnum.ADMIN);
                    flag3 = true;
                }
            }
        }

        if (!Integer.valueOf(selectedValue1).equals(localSettings.getGeneral().getAWS())) {
            localSettings.getGeneral().setAWS(Integer.valueOf(selectedValue1));
            flag3 = true;
        }

        switch (selectedValue2) {
            case "Русский" -> {
                if (!selectedValue2.equals(localSettings.getGeneral().getLanguage().getLanguageName())) {
                    localSettings.getGeneral().setLanguage(LanguageEnum.RUSSIAN);
                    flag3 = true;
                }
            }
            case "Английский" -> {
                if (!selectedValue2.equals(localSettings.getGeneral().getLanguage().getLanguageName())) {
                    localSettings.getGeneral().setLanguage(LanguageEnum.ENGLISH);
                    flag3 = true;
                }
            }
        }

        switch (selectedValue3) {
            case "DD.dddddd" -> {
                if (!selectedValue3.equals(localSettings.getGeneral().getCoordinateView().getCoordinateName())) {
                    localSettings.getGeneral().setCoordinateView(CoordinateViewEnum.DDDDDDDD);
                    flag3 = true;
                }
            }
            case "DD MM.mmmm" -> {
                if (!selectedValue3.equals(localSettings.getGeneral().getCoordinateView().getCoordinateName())) {
                    localSettings.getGeneral().setCoordinateView(CoordinateViewEnum.DDMMMMMM);
                    flag3 = true;
                }
            }
            case "DD MM SS.ss" -> {
                if (!selectedValue3.equals(localSettings.getGeneral().getCoordinateView().getCoordinateName())) {
                    localSettings.getGeneral().setCoordinateView(CoordinateViewEnum.DDMMSSSS);
                    flag3 = true;
                }
            }
        }
    }

    public void OnUpdateLocalMap() {
        if (!MapPath.getText().equals(localSettings.getMap().getMapPath())) {
            localSettings.getMap().setMapPath(MapPath.getText());
            flag3 = true;
        }
        if (!MatrixPath.getText().equals(localSettings.getMap().getMatrixPath())) {
            localSettings.getMap().setMatrixPath(MatrixPath.getText());
            flag3 = true;
        }
        if (!MapDFPath.getText().equals(localSettings.getMap().getMapDFPath())) {
            localSettings.getMap().setMapDFPath(MapDFPath.getText());
            flag3 = true;
        }
        if (!MatrixDFPath.getText().equals(localSettings.getMap().getMatrixDFPath())) {
            localSettings.getMap().setMatrixDFPath(MatrixDFPath.getText());
            flag3 = true;
        }
        if (!Integer.valueOf(DistanceBearingLine.getText()).equals(localSettings.getMap().getDistanceBearingLine())) {
            localSettings.getMap().setDistanceBearingLine(Integer.parseInt(DistanceBearingLine.getText()));
            flag3 = true;
        }
    }

    public void OnUpdateLocalDB() {
        try {
            Scanner sc = new Scanner(DBIPAddress.getText());
            String s = sc.next();

            char[] ch = s.toCharArray();

            int count = 0;
            for (char c : ch) {
                if (c == '.') { //перебираю совпадения и заношу в счетчик
                    count++;
                }
            }

            String[] words = DBIPAddress.getText().split("\\.");
            int[] intwords = new int[words.length];

            for (int i = 0; i < words.length; i++) {
                intwords[i] = Integer.parseInt(words[i]);
            }

            for (int i = 0; i < words.length; i++) {
                if (intwords[i] < 0 || intwords[i] > 255 || intwords.length != 4 || count != 3) {
                    DBIPAddress.setStyle("-fx-border-color: red");
                    flag1 = true;
                }
            }
            if (!DBIPAddress.getStyle().equals("-fx-border-color: red")) {
                if (!DBIPAddress.getText().equals(localSettings.getDb().getIPAddress())) {
                    localSettings.getDb().setIPAddress(DBIPAddress.getText());
                    flag3 = true;
                }
            }

        } catch (Exception exception11) {
            DBIPAddress.setStyle("-fx-border-color: red");
            flag1 = true;
        }

        try {
            if (!Integer.valueOf(DBPort.getText()).equals(localSettings.getDb().getPort())) {
                localSettings.getDb().setPort(Integer.parseInt(DBPort.getText()));
                flag3 = true;
            }
        } catch (Exception ex) {
            DBPort.setStyle("-fx-border-color: red");
            flag1 = true;
        }
    }

    public void OnUpdateLocalDF() {
        try {
            Scanner sc = new Scanner(DFIPAddress.getText());
            String s = sc.next();

            char[] ch = s.toCharArray();

            int count = 0;
            for (char c : ch) {
                if (c == '.') { //перебираю совпадения и заношу в счетчик
                    count++;
                }
            }

            String[] words = DFIPAddress.getText().split("\\.");
            int[] intwords = new int[words.length];

            for (int i = 0; i < intwords.length; i++) {
                intwords[i] = Integer.parseInt(words[i]);
            }

            for (int intword : intwords) {
                if (intword < 0 || intword > 255 || intwords.length != 4 || count != 3) {
                    DFIPAddress.setStyle("-fx-border-color: red");
                    flag1 = true;
                }
            }

            if (!DFIPAddress.getStyle().equals("-fx-border-color: red")) {
                if (!DFIPAddress.getText().equals(localSettings.getDf().getIPAddress())) {
                    localSettings.getDf().setIPAddress(DFIPAddress.getText());
                    flag3 = true;
                }
            }
        } catch (NumberFormatException exception12) {
            DFIPAddress.setStyle("-fx-border-color: red");
            flag1 = true;
        }

        try {
            if (!Integer.valueOf(DFPort.getText()).equals(localSettings.getDf().getPort())) {
                localSettings.getDf().setPort(Integer.parseInt(DFPort.getText()));
                flag3 = true;
            }
        } catch (Exception ex) {
            DFPort.setStyle("-fx-border-color: red");
            flag1 = true;
        }
    }

    public void OnUpdateLocalED() {
        selectedValue4 = EDTypeBox.getSelectionModel().getSelectedItem();
        if (selectedValue4.equals(TypeCntEnum.Modem_4wire.getCntName())) {

            if (!TypeCntEnum.Modem_4wire.getCntType().equals(localSettings.getEd().getType())) {
                localSettings.getEd().setType(TypeCntEnum.Modem_4wire.getCntType());
                flag3 = true;
            }

            try {
                Scanner sc = new Scanner(EDLocalIPModem_4wire.getText());
                String s = sc.next();

                char[] ch = s.toCharArray();

                int count = 0;
                for (char c : ch) {
                    if (c == '.') { //перебираю совпадения и заношу в счетчик
                        count++;
                    }
                }

                String[] words = EDLocalIPModem_4wire.getText().split("\\.");
                int[] intwords = new int[words.length];

                for (int i = 0; i < words.length; i++) {
                    intwords[i] = Integer.parseInt(words[i]);
                }

                for (int i = 0; i < words.length; i++) {
                    if (intwords[i] < 0 || intwords[i] > 255 || intwords.length != 4 || count != 3) {
                        EDLocalIPModem_4wire.setStyle("-fx-border-color: red");
                        flag1 = true;
                    }
                }

                if (!EDLocalIPModem_4wire.getStyle().equals("-fx-border-color: red"))
                    if (!EDLocalIPModem_4wire.getText().equals(localSettings.getEd().getModem_4wire().getLocal().getIPAddress())) {
                        localSettings.getEd().getModem_4wire().getLocal().setIPAddress(EDLocalIPModem_4wire.getText());
                        flag3 = true;
                    }
            } catch (NumberFormatException exception11) {
                EDLocalIPModem_4wire.setStyle("-fx-border-color: red");
                flag1 = true;
            }

            try {
                if (!Integer.valueOf(EDLocalPortModem_4wire.getText()).equals(localSettings.getEd().getModem_4wire().getLocal().getPort())) {
                    localSettings.getEd().getModem_4wire().getLocal().setPort(Integer.parseInt(EDLocalPortModem_4wire.getText()));
                    flag3 = true;
                }
            } catch (Exception ex) {
                EDLocalPortModem_4wire.setStyle("-fx-border-color: red");
                flag1 = true;
            }

            try {
                Scanner sc = new Scanner(EDRemoteIPModem_4wire.getText());
                String s = sc.next();

                char[] ch = s.toCharArray();

                int count = 0;
                for (char c : ch) {
                    if (c == '.') { //перебираю совпадения и заношу в счетчик
                        count++;
                    }
                }

                String[] words = EDRemoteIPModem_4wire.getText().split("\\.");
                int[] intwords = new int[words.length];

                for (int i = 0; i < words.length; i++) {
                    intwords[i] = Integer.parseInt(words[i]);
                }

                for (int i = 0; i < words.length; i++) {
                    if (intwords[i] < 0 || intwords[i] > 255 || intwords.length != 4 || count != 3) {
                        EDRemoteIPModem_4wire.setStyle("-fx-border-color: red");
                        flag1 = true;
                    }
                }
                if (!EDRemoteIPModem_4wire.getStyle().equals("-fx-border-color: red"))
                    if (!EDRemoteIPModem_4wire.getText().equals(localSettings.getEd().getModem_4wire().getRemote().getIPAddress())) {
                        localSettings.getEd().getModem_4wire().getRemote().setIPAddress((EDRemoteIPModem_4wire.getText()));
                        flag3 = true;
                    }

            } catch (NumberFormatException exception11) {
                EDRemoteIPModem_4wire.setStyle("-fx-border-color: red");
                flag1 = true;
            }

            try {
                if (!Integer.valueOf(EDRemotePortModem_4wire.getText()).equals(localSettings.getEd().getModem_4wire().getRemote().getPort())) {
                    localSettings.getEd().getModem_4wire().getRemote().setPort((Integer.parseInt(EDRemotePortModem_4wire.getText())));
                    flag3 = true;
                }
            } catch (Exception ex) {
                EDRemotePortModem_4wire.setStyle("-fx-border-color: red");
                flag1 = true;
            }

            localSettings.getEd().getRadiomodem().getLocal().setIPAddress(EDLocalIPRadiomodem.getText());
            localSettings.getEd().getRadiomodem().getLocal().setPort(Integer.parseInt(EDLocalPortRadiomodem.getText()));
            localSettings.getEd().getRadiomodem().getRemote().setIPAddress(EDRemoteIPRadiomodem.getText());
            localSettings.getEd().getRadiomodem().getRemote().setPort(Integer.parseInt(EDRemotePortRadiomodem.getText()));

            localSettings.getEd().getRouter3G_4G().getLocal().setIPAddress(EDLocalIP3G_4G.getText());
            localSettings.getEd().getRouter3G_4G().getLocal().setPort(Integer.parseInt(EDLocalPort3G_4G.getText()));
            localSettings.getEd().getRouter3G_4G().getRemote().setIPAddress(EDRemoteIP3G_4G.getText());
            localSettings.getEd().getRouter3G_4G().getRemote().setPort(Integer.parseInt(EDRemotePort3G_4G.getText()));

        } else if (selectedValue4.equals(TypeCntEnum.Radiomodem.getCntName())) {

            if (!TypeCntEnum.Radiomodem.getCntType().equals(localSettings.getEd().getType())) {
                localSettings.getEd().setType(TypeCntEnum.Radiomodem.getCntType());
                flag3 = true;
            }

            localSettings.getEd().getModem_4wire().getLocal().setIPAddress(EDLocalIPModem_4wire.getText());
            localSettings.getEd().getModem_4wire().getLocal().setPort(Integer.parseInt(EDLocalPortModem_4wire.getText()));
            localSettings.getEd().getModem_4wire().getRemote().setIPAddress(EDRemoteIPModem_4wire.getText());
            localSettings.getEd().getModem_4wire().getRemote().setPort(Integer.parseInt(EDRemotePortModem_4wire.getText()));

            try {
                Scanner sc = new Scanner(EDLocalIPRadiomodem.getText());
                String s = sc.next();

                char[] ch = s.toCharArray();

                int count = 0;
                for (char c : ch) {
                    if (c == '.') { //перебираю совпадения и заношу в счетчик
                        count++;
                    }
                }

                String[] words = EDLocalIPRadiomodem.getText().split("\\.");
                int[] intwords = new int[words.length];

                for (int i = 0; i < words.length; i++) {
                    intwords[i] = Integer.parseInt(words[i]);
                }

                for (int i = 0; i < words.length; i++) {
                    if (intwords[i] < 0 || intwords[i] > 255 || intwords.length != 4 || count != 3) {
                        EDLocalIPRadiomodem.setStyle("-fx-border-color: red");
                        flag1 = true;
                    }
                }
                if (!EDLocalIPRadiomodem.getStyle().equals("-fx-border-color: red"))
                    if (!EDLocalIPRadiomodem.getText().equals(localSettings.getEd().getRadiomodem().getLocal().getIPAddress())) {
                        localSettings.getEd().getRadiomodem().getLocal().setIPAddress(EDLocalIPRadiomodem.getText());
                        flag3 = true;
                    }
            } catch (NumberFormatException exception11) {
                EDLocalIPRadiomodem.setStyle("-fx-border-color: red");
                flag1 = true;
            }

            try {
                if (!Integer.valueOf(EDLocalPortRadiomodem.getText()).equals(localSettings.getEd().getRadiomodem().getLocal().getPort())) {
                    localSettings.getEd().getRadiomodem().getLocal().setPort(Integer.parseInt(EDLocalPortRadiomodem.getText()));
                    flag3 = true;
                }
            } catch (Exception ex) {
                EDLocalPortRadiomodem.setStyle("-fx-border-color: red");
                flag1 = true;
            }

            try {
                Scanner sc = new Scanner(EDRemoteIPRadiomodem.getText());
                String s = sc.next();

                char[] ch = s.toCharArray();

                int count = 0;
                for (char c : ch) {
                    if (c == '.') { //перебираю совпадения и заношу в счетчик
                        count++;
                    }
                }

                String[] words = EDRemoteIPRadiomodem.getText().split("\\.");
                int[] intwords = new int[words.length];

                for (int i = 0; i < words.length; i++) {
                    intwords[i] = Integer.parseInt(words[i]);
                }

                for (int i = 0; i < words.length; i++) {
                    if (intwords[i] < 0 || intwords[i] > 255 || intwords.length != 4 || count != 3) {
                        EDRemoteIPRadiomodem.setStyle("-fx-border-color: red");
                        flag1 = true;
                    }
                }

                if (!EDRemoteIPRadiomodem.getStyle().equals("-fx-border-color: red"))
                    if (!EDRemoteIPRadiomodem.getText().equals(localSettings.getEd().getRadiomodem().getRemote().getIPAddress())) {
                        localSettings.getEd().getRadiomodem().getRemote().setIPAddress(EDRemoteIPRadiomodem.getText());
                        flag3 = true;
                    }

            } catch (NumberFormatException exception11) {
                EDRemoteIPRadiomodem.setStyle("-fx-border-color: red");
                flag1 = true;
            }

            try {
                if (!Integer.valueOf(EDRemotePortRadiomodem.getText()).equals(localSettings.getEd().getRadiomodem().getRemote().getPort())) {
                    localSettings.getEd().getRadiomodem().getRemote().setPort(Integer.parseInt(EDRemotePortRadiomodem.getText()));
                    flag3 = true;
                }
            } catch (Exception ex) {
                EDRemotePortRadiomodem.setStyle("-fx-border-color: red");
                flag1 = true;
            }


            localSettings.getEd().getRouter3G_4G().getLocal().setIPAddress(EDLocalIP3G_4G.getText());
            localSettings.getEd().getRouter3G_4G().getLocal().setPort(Integer.parseInt(EDLocalPort3G_4G.getText()));
            localSettings.getEd().getRouter3G_4G().getRemote().setIPAddress(EDRemoteIP3G_4G.getText());
            localSettings.getEd().getRouter3G_4G().getRemote().setPort(Integer.parseInt(EDRemotePort3G_4G.getText()));

        } else if (selectedValue4.equals(TypeCntEnum.Router_3G_4G.getCntName())) {

            if (!TypeCntEnum.Router_3G_4G.getCntType().equals(localSettings.getEd().getType())) {
                localSettings.getEd().setType(TypeCntEnum.Router_3G_4G.getCntType());
                flag3 = true;
            }
            localSettings.getEd().getModem_4wire().getLocal().setIPAddress(EDLocalIPModem_4wire.getText());
            localSettings.getEd().getModem_4wire().getLocal().setPort(Integer.parseInt(EDLocalPortModem_4wire.getText()));
            localSettings.getEd().getModem_4wire().getRemote().setIPAddress(EDRemoteIPModem_4wire.getText());
            localSettings.getEd().getModem_4wire().getRemote().setPort(Integer.parseInt(EDRemotePortModem_4wire.getText()));

            localSettings.getEd().getRadiomodem().getLocal().setIPAddress(EDLocalIPRadiomodem.getText());
            localSettings.getEd().getRadiomodem().getLocal().setPort(Integer.parseInt(EDLocalPortRadiomodem.getText()));
            localSettings.getEd().getRadiomodem().getRemote().setIPAddress(EDRemoteIPRadiomodem.getText());
            localSettings.getEd().getRadiomodem().getRemote().setPort(Integer.parseInt(EDRemotePortRadiomodem.getText()));

            try {
                Scanner sc = new Scanner(EDLocalIP3G_4G.getText());
                String s = sc.next();

                char[] ch = s.toCharArray();

                int count = 0;
                for (char c : ch) {
                    if (c == '.') { //перебираю совпадения и заношу в счетчик
                        count++;
                    }
                }

                String[] words = EDLocalIP3G_4G.getText().split("\\.");
                int[] intwords = new int[words.length];

                for (int i = 0; i < words.length; i++) {
                    intwords[i] = Integer.parseInt(words[i]);
                }

                for (int i = 0; i < words.length; i++) {
                    if (intwords[i] < 0 || intwords[i] > 255 || intwords.length != 4 || count != 3) {
                        EDLocalIP3G_4G.setStyle("-fx-border-color: red");
                        flag1 = true;
                    }
                }
                if (!EDLocalIP3G_4G.getStyle().equals("-fx-border-color: red"))
                    if (!EDLocalIP3G_4G.getText().equals(localSettings.getEd().getRouter3G_4G().getLocal().getIPAddress())) {
                        localSettings.getEd().getRouter3G_4G().getLocal().setIPAddress(EDLocalIP3G_4G.getText());
                        flag3 = true;
                    }
            } catch (NumberFormatException exception11) {
                EDLocalIP3G_4G.setStyle("-fx-border-color: red");
                flag1 = true;
            }

            try {
                if (!Integer.valueOf(EDLocalPort3G_4G.getText()).equals(localSettings.getEd().getRouter3G_4G().getLocal().getPort())) {
                    localSettings.getEd().getRouter3G_4G().getLocal().setPort(Integer.parseInt(EDLocalPort3G_4G.getText()));
                    flag3 = true;
                }
            } catch (Exception ex) {
                EDLocalPort3G_4G.setStyle("-fx-border-color: red");
                flag1 = true;
            }

            try {
                Scanner sc = new Scanner(EDRemoteIP3G_4G.getText());
                String s = sc.next();

                char[] ch = s.toCharArray();

                int count = 0;
                for (char c : ch) {
                    if (c == '.') { //перебираю совпадения и заношу в счетчик
                        count++;
                    }
                }

                String[] words = EDRemoteIP3G_4G.getText().split("\\.");
                int[] intwords = new int[words.length];

                for (int i = 0; i < words.length; i++) {
                    intwords[i] = Integer.parseInt(words[i]);
                }

                for (int i = 0; i < words.length; i++) {
                    if (intwords[i] < 0 || intwords[i] > 255 || intwords.length != 4 || count != 3) {
                        EDRemoteIP3G_4G.setStyle("-fx-border-color: red");
                        flag1 = true;
                    }
                }
                if (!EDRemoteIP3G_4G.getStyle().equals("-fx-border-color: red"))
                    if (!EDRemoteIP3G_4G.getText().equals(localSettings.getEd().getRouter3G_4G().getRemote().getIPAddress())) {
                        localSettings.getEd().getRouter3G_4G().getRemote().setIPAddress(EDRemoteIP3G_4G.getText());
                        flag3 = true;
                    }
            } catch (NumberFormatException exception11) {
                EDRemoteIP3G_4G.setStyle("-fx-border-color: red");
                flag1 = true;
            }

            try {
                if (!Integer.valueOf(EDRemotePort3G_4G.getText()).equals(localSettings.getEd().getRouter3G_4G().getRemote().getPort())) {
                    localSettings.getEd().getRouter3G_4G().getRemote().setPort(Integer.parseInt(EDRemotePort3G_4G.getText()));
                    flag3 = true;
                }
            } catch (Exception ex) {
                EDRemotePort3G_4G.setStyle("-fx-border-color: red");
                flag1 = true;
            }
        }
    }

    public void OnUpdateLocalPC() {
        selectedValue5 = PCTypeBox.getSelectionModel().getSelectedItem();
        if (selectedValue5.equals(TypeCntEnum.Modem_4wire.getCntName())) {

            if (!TypeCntEnum.Modem_4wire.getCntType().equals(localSettings.getPc().getType())) {
                localSettings.getPc().setType(TypeCntEnum.Modem_4wire.getCntType());
                flag3 = true;
            }
            try {
                Scanner sc = new Scanner(PCRemoteIPModem_4wire.getText());
                String s = sc.next();

                char[] ch = s.toCharArray();

                int count = 0;
                for (char c : ch) {
                    if (c == '.') { //перебираю совпадения и заношу в счетчик
                        count++;
                    }
                }

                String[] words = PCRemoteIPModem_4wire.getText().split("\\.");
                int[] intwords = new int[words.length];

                for (int i = 0; i < words.length; i++) {
                    intwords[i] = Integer.parseInt(words[i]);
                }

                for (int i = 0; i < words.length; i++) {
                    if (intwords[i] < 0 || intwords[i] > 255 || intwords.length != 4 || count != 3) {
                        PCRemoteIPModem_4wire.setStyle("-fx-border-color: red");
                        flag1 = true;
                    }
                }

                if (!PCRemoteIPModem_4wire.getStyle().equals("-fx-border-color: red"))
                    if (!PCRemoteIPModem_4wire.getText().equals(localSettings.getPc().getModem_4wire().getIPAddress())) {
                        localSettings.getPc().getModem_4wire().setIPAddress(PCRemoteIPModem_4wire.getText());
                        flag3 = true;
                    }

            } catch (NumberFormatException exception11) {
                PCRemoteIPModem_4wire.setStyle("-fx-border-color: red");
                flag1 = true;
            }

            try {
                if (!Integer.valueOf(PCRemotePortModem_4wire.getText()).equals(localSettings.getPc().getModem_4wire().getPort())) {
                    localSettings.getPc().getModem_4wire().setPort(Integer.parseInt(PCRemotePortModem_4wire.getText()));
                    flag3 = true;
                }
            } catch (Exception ex) {
                PCRemotePortModem_4wire.setStyle("-fx-border-color: red");
                flag1 = true;
            }

            localSettings.getPc().getRadiomodem().setIPAddress(PCRemoteIPRadiomodem.getText());
            localSettings.getPc().getRadiomodem().setPort(Integer.parseInt(PCRemotePortRadiomodem.getText()));

        } else if (selectedValue5.equals(TypeCntEnum.Radiomodem.getCntName())) {

            if (!TypeCntEnum.Radiomodem.getCntType().equals(localSettings.getPc().getType())) {
                localSettings.getPc().setType(TypeCntEnum.Radiomodem.getCntType());
                flag3 = true;
            }
            localSettings.getPc().getModem_4wire().setIPAddress(PCRemoteIPModem_4wire.getText());
            localSettings.getPc().getModem_4wire().setPort(Integer.parseInt(PCRemotePortModem_4wire.getText()));

            try {
                Scanner sc = new Scanner(PCRemoteIPRadiomodem.getText());
                String s = sc.next();

                char[] ch = s.toCharArray();

                int count = 0;
                for (char c : ch) {
                    if (c == '.') { //перебираю совпадения и заношу в счетчик
                        count++;
                    }
                }

                String[] words = PCRemoteIPRadiomodem.getText().split("\\.");
                int[] intwords = new int[words.length];

                for (int i = 0; i < words.length; i++) {
                    intwords[i] = Integer.parseInt(words[i]);
                }

                for (int i = 0; i < words.length; i++) {
                    if (intwords[i] < 0 || intwords[i] > 255 || intwords.length != 4 || count != 3) {
                        PCRemoteIPRadiomodem.setStyle("-fx-border-color: red");
                        flag1 = true;
                    }
                }

                if (!PCRemoteIPRadiomodem.getStyle().equals("-fx-border-color: red"))
                    if (!PCRemoteIPRadiomodem.getText().equals(localSettings.getPc().getRadiomodem().getIPAddress())) {
                        localSettings.getPc().getRadiomodem().setIPAddress(PCRemoteIPRadiomodem.getText());
                        flag3 = true;
                    }

            } catch (NumberFormatException exception11) {
                PCRemoteIPRadiomodem.setStyle("-fx-border-color: red");
                flag1 = true;
            }

            try {
                if (!Integer.valueOf(PCRemotePortRadiomodem.getText()).equals(localSettings.getPc().getRadiomodem().getPort())) {
                    localSettings.getPc().getRadiomodem().setPort(Integer.parseInt(PCRemotePortRadiomodem.getText()));
                    flag3 = true;
                }
            } catch (Exception ex) {
                PCRemotePortRadiomodem.setStyle("-fx-border-color: red");
                flag1 = true;
            }
        }
    }

    public void OnUpdateLocalBPSS() {
        selectedValue7 = SpeedBox.getSelectionModel().getSelectedItem();

        if (SerialPort.getCommPorts().length != 0) {
            selectedValue6 = ComBox.getSelectionModel().getSelectedItem();
            if (!selectedValue6.equals(localSettings.getBpss().getCom_Port())) {
                localSettings.getBpss().setCom_Port(ComBox.getValue());
                flag3 = true;
            }
        }

        if (!Integer.valueOf(selectedValue7).equals(localSettings.getBpss().getSpeed())) {
            localSettings.getBpss().setSpeed(Integer.parseInt(SpeedBox.getValue()));
            flag3 = true;
        }
    }

    public void OnUpdateLocalSound() {
        selectedValue8 = SoundBox.getSelectionModel().getSelectedItem();
        if (!selectedValue8.equals(localSettings.getSound().getSound())) {
            localSettings.getSound().setSound(selectedValue8);
            flag3 = true;
        }

        selectedValue11 = AdjustmentLevelBox.getSelectionModel().getSelectedItem();
        if (selectedValue11.equals(AdjustmentLevelEnum.AUTO.getAccessName())) {
            if (!AdjustmentLevelEnum.AUTO.getAccessType().equals(localSettings.getSound().getType())) {
                localSettings.getSound().setType(AdjustmentLevelEnum.AUTO.getAccessType());
                flag3 = true;
            }
        } else if (selectedValue11.equals(AdjustmentLevelEnum.MANUAL.getAccessName())) {
            if (!AdjustmentLevelEnum.MANUAL.getAccessType().equals(localSettings.getSound().getType())) {
                localSettings.getSound().setType(AdjustmentLevelEnum.MANUAL.getAccessType());
                flag3 = true;
            }
        }

        try {
            if (Double.parseDouble(AGLLevel.getText()) < 0 || Double.parseDouble(AGLLevel.getText()) > 1) {
                AGLLevel.setStyle("-fx-border-color: red");
                flag1 = true;
            }

            if (!AGLLevel.getStyle().equals("-fx-border-color: red"))
                if (Double.parseDouble(AGLLevel.getText()) != localSettings.getSound().getAGLLevel()) {
                    localSettings.getSound().setAGLLevel(Double.parseDouble(AGLLevel.getText()));
                    flag3 = true;
                }

        } catch (Exception exception11) {
            AGLLevel.setStyle("-fx-border-color: red");
            flag1 = true;
        }

        try {
            if (Double.parseDouble(MGCFactor.getText()) < 0 || Double.parseDouble(MGCFactor.getText()) > 10) {
                MGCFactor.setStyle("-fx-border-color: red");
                flag1 = true;
            }

            if (!MGCFactor.getStyle().equals("-fx-border-color: red"))
                if (Double.parseDouble(MGCFactor.getText()) != localSettings.getSound().getMGCFactor()) {
                    localSettings.getSound().setMGCFactor(Double.parseDouble(MGCFactor.getText()));
                    flag3 = true;
                }

        } catch (Exception exception11) {
            MGCFactor.setStyle("-fx-border-color: red");
            flag1 = true;
        }

//        if (AudioSystem.getMixerInfo().length != 0) {
//            selectedValue9 = TADeviceBox.getSelectionModel().getSelectedItem();
//            selectedValue10 = ListeningDeviceBox.getSelectionModel().getSelectedItem();
//            if (!selectedValue9.equals(localSettings.getSound().getTADevice())) {
//                localSettings.getSound().setTADevice(TADeviceBox.getValue());
//                flag3 = true;
//            }
//            if (!selectedValue10.equals(localSettings.getSound().getListeningDevice())) {
//                localSettings.getSound().setListeningDevice(ListeningDeviceBox.getValue());
//                flag3 = true;
//            }
//        }


    }

    public void UpdateComboBox(ObservableList<Integer> list) {
        this.SoundList = list;
        SoundBox.setItems(SoundList);
        SoundBox.getSelectionModel().select(0);
    }

    public void captureAudio() {
        try {
            Mixer.Info[] mixerInfo = AudioSystem.getMixerInfo();
            for (Mixer.Info info : mixerInfo) {
                TADeviceList.add(info.getName());
                ListeningDeviceList.add(info.getName());
            }
        } catch (Exception ignored) {
        }
    }

    public void OnUpdateLocal() {
        flag1 = false;
        flag3 = false;

        OnUpdateLocalGeneral();
        OnUpdateLocalMap();
        OnUpdateLocalDB();
        OnUpdateLocalDF();
        OnUpdateLocalED();
        OnUpdateLocalPC();
        OnUpdateLocalBPSS();
        OnUpdateLocalSound();
        /** ----------LOCAL SETTINGS---------- */

        if (!flag1 && flag3) {

            for (IPropertiesEvent propertiesEvent : listeners)
                propertiesEvent.OnLocalProperties(localSettings);
        }
    }

    public void OnUpdateGlobal() {
        flag2 = false;
        flag3 = false;

        try {
            int JAMMINGTIME = Integer.parseInt(JammingTime.getText());
            if (JAMMINGTIME < 0 || JAMMINGTIME > 1000) {
                JammingTime.setStyle("-fx-border-color: red");
                flag2 = true;
            }
            if (!JammingTime.getStyle().equals("-fx-border-color: red")) {
                if (!Integer.valueOf(JammingTime.getText()).equals((globalPropertiesModel.getTimeJamming()))) {
                    globalPropertiesModel.setTimeJamming(JAMMINGTIME);
                    flag3 = true;
                }
            }
        } catch (Exception exception1) {
            JammingTime.setStyle("-fx-border-color: red");
        }

        try {
            int NOPA = Integer.parseInt(NoPA.getText());
            if (NOPA < 1 || NOPA > 20) {
                NoPA.setStyle("-fx-border-color: red");
                flag2 = true;
            }
            if (!NoPA.getStyle().equals("-fx-border-color: red")) {
                if (!Integer.valueOf(NoPA.getText()).equals((globalPropertiesModel.getNumberOfPhasesAveragings()))) {
                    globalPropertiesModel.setNumberOfPhasesAveragings(NOPA);
                    flag3 = true;
                }
            }
        } catch (Exception exception1) {
            NoPA.setStyle("-fx-border-color: red");
        }

        try {
            int NOBA = Integer.parseInt(NoBA.getText());
            if (NOBA < 1 || NOBA > 20) {
                NoBA.setStyle("-fx-border-color: red");
                flag2 = true;
            }
            if (!NoBA.getStyle().equals("-fx-border-color: red")) {
                if (!Integer.valueOf(NoBA.getText()).equals((globalPropertiesModel.getNumberOfBearingAveragings()))) {
                    globalPropertiesModel.setNumberOfBearingAveragings(NOBA);
                    flag3 = true;
                }
            }
        } catch (Exception exception1) {
            NoBA.setStyle("-fx-border-color: red");
        }

        try {
            int COURSEANGLE = Integer.parseInt(CourseAngle.getText());
            if (COURSEANGLE < 1 || COURSEANGLE > 360) {
                CourseAngle.setStyle("-fx-border-color: red");
                flag2 = true;
            }
            if (!CourseAngle.getStyle().equals("-fx-border-color: red")) {
                if (!Integer.valueOf(CourseAngle.getText()).equals((globalPropertiesModel.getCourseAngle()))) {
                    globalPropertiesModel.setCourseAngle(COURSEANGLE);
                    flag3 = true;
                }
            }
        } catch (Exception exception1) {
            CourseAngle.setStyle("-fx-border-color: red");
        }

        try {
            int ADAPTIVETHRESHOLDFHSS = Integer.parseInt(AdaptiveThresholdFhss.getText());
            if (ADAPTIVETHRESHOLDFHSS < 0 || ADAPTIVETHRESHOLDFHSS > 200) {
                AdaptiveThresholdFhss.setStyle("-fx-border-color: red");
                flag2 = true;
            }
            if (!AdaptiveThresholdFhss.getStyle().equals("-fx-border-color: red")) {
                if (!Integer.valueOf(AdaptiveThresholdFhss.getText()).equals((globalPropertiesModel.getAdaptiveThresholdFhss()))) {
                    globalPropertiesModel.setAdaptiveThresholdFhss(ADAPTIVETHRESHOLDFHSS);
                    flag3 = true;
                }
            }
        } catch (Exception exception1) {
            AdaptiveThresholdFhss.setStyle("-fx-border-color: red");
        }

        try {
            int Channel = Integer.parseInt(ChannelLitter.getText());
            if (Channel < 1 || Channel > 4) {
                ChannelLitter.setStyle("-fx-border-color: red");
                flag2 = true;
            }
            if (!ChannelLitter.getStyle().equals("-fx-border-color: red")) {
                if (!Integer.valueOf(ChannelLitter.getText()).equals(globalPropertiesModel.getNumberOfChannelsInLetter())) {
                    globalPropertiesModel.setNumberOfChannelsInLetter(Channel);
                    flag3 = true;
                }
            }
        } catch (Exception exception1) {
            ChannelLitter.setStyle("-fx-border-color: red");
        }

        try {
            int IRI = Integer.parseInt(IRILitter.getText());
            if (IRI < 1 || IRI > 20) {
                IRILitter.setStyle("-fx-border-color: red");
                flag2 = true;
            }
            if (!IRILitter.getStyle().equals("-fx-border-color: red")) {
                if (!Integer.valueOf(IRILitter.getText()).equals((globalPropertiesModel.getNumberOfResInLetter()))) {
                    globalPropertiesModel.setNumberOfResInLetter(IRI);
                    flag3 = true;
                }
            }
        } catch (Exception exception1) {
            IRILitter.setStyle("-fx-border-color: red");
        }

        try {
            int LSRLTIME = Integer.parseInt(LSRLTime.getText());
            if (LSRLTIME < 1 || LSRLTIME > 20) {
                LSRLTime.setStyle("-fx-border-color: red");
                flag2 = true;
            }
            if (!LSRLTime.getStyle().equals("-fx-border-color: red")) {
                if (!Integer.valueOf(LSRLTime.getText()).equals((globalPropertiesModel.getLongWorkingSignalDurationMs()))) {
                    globalPropertiesModel.setLongWorkingSignalDurationMs(LSRLTIME);
                    flag3 = true;
                }
            }
        } catch (Exception exception1) {
            LSRLTime.setStyle("-fx-border-color: red");
        }

        try {
            byte PRIORITY = Byte.parseByte(Priority.getText());
            if (PRIORITY < 1 || PRIORITY > 9) {
                Priority.setStyle("-fx-border-color: red");
                flag2 = true;
            }
            if (!Priority.getStyle().equals("-fx-border-color: red")) {
                if (!Byte.valueOf(Priority.getText()).equals((globalPropertiesModel.getPriority()))) {
                    globalPropertiesModel.setPriority(PRIORITY);
                    flag3 = true;
                }
            }
        } catch (Exception exception1) {
            Priority.setStyle("-fx-border-color: red");
        }

        try {
            int THRESHOLD = Integer.parseInt(Threshold.getText());
            if (THRESHOLD < -130 || THRESHOLD > 0) {
                Threshold.setStyle("-fx-border-color: red");
                flag2 = true;
            }
            if (!Threshold.getStyle().equals("-fx-border-color: red")) {
                if (!Integer.valueOf(Threshold.getText()).equals((globalPropertiesModel.getThreshold()))) {
                    globalPropertiesModel.setThreshold(THRESHOLD);
                    flag3 = true;
                }
            }
        } catch (Exception exception1) {
            Threshold.setStyle("-fx-border-color: red");
        }

        try {
            int RADIATIONTIME = Integer.parseInt(RadiationTime.getText());
            if (RADIATIONTIME < 0 || RADIATIONTIME > 18000) {
                RadiationTime.setStyle("-fx-border-color: red");
                flag2 = true;
            }
            if (!RadiationTime.getStyle().equals("-fx-border-color: red")) {
                if (!Integer.valueOf(RadiationTime.getText()).equals(globalPropertiesModel.getTimeRadiationFF())) {
                    globalPropertiesModel.setTimeRadiationFF(RADIATIONTIME);
                    flag3 = true;
                }
            }
        } catch (Exception exception1) {
            RadiationTime.setStyle("-fx-border-color: red");
        }

        if (TestCheck.isSelected() != globalPropertiesModel.getIsTest()) {
            globalPropertiesModel.setIsTest(TestCheck.isSelected());
            flag3 = true;
        }

        selectedValue20 = FFTResolutionBox.getSelectionModel().getSelectedItem();

        switch (selectedValue20) {
            case "16384" -> {
                if (!Byte.valueOf(FftResolution.N16384.getFftResolutionType()).equals(globalPropertiesModel.getFhssFftResolution().getFftResolutionType())) {
                    globalPropertiesModel.setFhssFftResolution(FftResolution.N16384);
                    flag3 = true;
                }
            }
            case "8192" -> {
                if (!Byte.valueOf(FftResolution.N8192.getFftResolutionType()).equals(globalPropertiesModel.getFhssFftResolution().getFftResolutionType())) {
                    globalPropertiesModel.setFhssFftResolution(FftResolution.N8192);
                    flag3 = true;
                }
            }
            case "4096" -> {
                if (!Byte.valueOf(FftResolution.N4096.getFftResolutionType()).equals(globalPropertiesModel.getFhssFftResolution().getFftResolutionType())) {
                    globalPropertiesModel.setFhssFftResolution(FftResolution.N4096);
                    flag3 = true;
                }
            }
        }

        try {
            int FHSSRADIATIONTIME = Integer.parseInt(FHSSRadiationTime.getText());
            if (FHSSRADIATIONTIME < 300 || FHSSRADIATIONTIME > 100000) {
                FHSSRadiationTime.setStyle("-fx-border-color: red");
                flag2 = true;
            }
            if (!FHSSRadiationTime.getStyle().equals("-fx-border-color: red")) {
                if (!Integer.valueOf(FHSSRadiationTime.getText()).equals(globalPropertiesModel.getTimeRadiationFHSS())) {
                    globalPropertiesModel.setTimeRadiationFHSS(FHSSRADIATIONTIME);
                    flag3 = true;
                }
            }
        } catch (Exception exception1) {
            FHSSRadiationTime.setStyle("-fx-border-color: red");
        }


//        try {
//            double SearchS = Double.parseDouble(SearchSector.getText());
//            if (SearchS < 2 || SearchS > 360) {
//                SearchSector.setStyle("-fx-border-color: red");
//                flag2 = true;
//            }
//            if (!SearchSector.getStyle().equals("-fx-border-color: red")) {
//                if (!Double.valueOf(SearchSector.getText()).equals(globalPropertiesModel.getSearchSector())) {
//                    globalPropertiesModel.setSearchSector(SearchS);
//                    flag3 = true;
//                }
//            }
//        } catch (Exception exception1) {
//            SearchSector.setStyle("-fx-border-color: red");
//        }
//
//        try {
//            int SearchT = Integer.parseInt(SearchTime.getText());
//            if (SearchT < 1 || SearchT > 18000) {
//                SearchTime.setStyle("-fx-border-color: red");
//                flag2 = true;
//            }
//            if (!SearchTime.getStyle().equals("-fx-border-color: red")) {
//                if (!Integer.valueOf(SearchTime.getText()).equals(globalPropertiesModel.getSearchTime())) {
//                    globalPropertiesModel.setSearchTime(SearchT);
//                    flag3 = true;
//                }
//
//            }
//        } catch (Exception exception1) {
//            SearchTime.setStyle("-fx-border-color: red");
//        }
//
//        try {
//            double Lat = Double.parseDouble(Latitude.getText());
//            if (Lat < -90 || Lat > 90) {
//                Latitude.setStyle("-fx-border-color: red");
//                flag2 = true;
//            }
//            if (!Latitude.getStyle().equals("-fx-border-color: red")) {
//                if (!Double.valueOf(Latitude.getText()).equals(globalPropertiesModel.getGnss().latitude)) {
//                    coord.latitude = Lat; //coord = new Coord(Lat, 0, 0);
//                    flag3 = true;
//                }
//            }
//        } catch (Exception exception1) {
//            Latitude.setStyle("-fx-border-color: red");
//        }
//
//        try {
//            double Long = Double.parseDouble(Longtitude.getText());
//            if (Long < -180 || Long > 180) {
//                Longtitude.setStyle("-fx-border-color: red");
//                flag2 = true;
//            }
//            if (!Longtitude.getStyle().equals("-fx-border-color: red")) {
//                if (!Double.valueOf(Longtitude.getText()).equals(globalPropertiesModel.getGnss().longitude)) {
//                    coord.longitude = Long; //coord = new Coord(0, Long, 0);
//                    flag3 = true;
//                }
//            }
//        } catch (Exception exception1) {
//            Longtitude.setStyle("-fx-border-color: red");
//        }
//
//        try {
//            float Alt = Float.parseFloat(Altitude.getText());
//            if (Alt < 0 || Alt > 1000) {
//                Altitude.setStyle("-fx-border-color: red");
//                flag2 = true;
//            }
//            if (!Altitude.getStyle().equals("-fx-border-color: red")) {
//                if (!Float.valueOf(Altitude.getText()).equals(globalPropertiesModel.getGnss().altitude)) {
//                    coord.altitude = Alt; //coord = new Coord(0, 0 , Alt);
//                    flag3 = true;
//                }
//            }
//        } catch (Exception exception1) {
//            Altitude.setStyle("-fx-border-color: red");
//        }
//
//        globalPropertiesModel.setGnss(coord);


        try {
            int PATIME = Integer.parseInt(PATime.getText());
            if (PATIME < 10 || PATIME > 300) {
                PATime.setStyle("-fx-border-color: red");
                flag2 = true;
            }
            if (!PATime.getStyle().equals("-fx-border-color: red")) {
                if (!Integer.valueOf(PATime.getText()).equals(globalPropertiesModel.getJsgAmPollInterval())) {
                    globalPropertiesModel.setJsgAmPollInterval(PATIME);
                    flag3 = true;
                }
            }
        } catch (Exception exception1) {
            PATime.setStyle("-fx-border-color: red");
        }

        selectedValue21 = PowerBox.getSelectionModel().getSelectedItem();

        switch (selectedValue21) {
            case "25" -> {
                if (!Byte.valueOf(PowerPercent.P25.getPowerPercentType()).equals(globalPropertiesModel.getJsgAmPower().getPowerPercentType())) {
                    globalPropertiesModel.setJsgAmPower(PowerPercent.P25);
                    flag3 = true;
                }
            }
            case "50" -> {
                if (!Byte.valueOf(PowerPercent.P50.getPowerPercentType()).equals(globalPropertiesModel.getJsgAmPower().getPowerPercentType())) {
                    globalPropertiesModel.setJsgAmPower(PowerPercent.P50);
                    flag3 = true;
                }
            }
            case "100" -> {
                if (!Byte.valueOf(PowerPercent.P100.getPowerPercentType()).equals(globalPropertiesModel.getJsgAmPower().getPowerPercentType())) {
                    globalPropertiesModel.setJsgAmPower(PowerPercent.P100);
                    flag3 = true;
                }
            }
        }


        if (!flag2 && flag3) {
            for (IPropertiesEvent propertiesEvent : listeners)
                propertiesEvent.OnGlobalProperties(globalPropertiesModel);
        }
    }

    public String[] getStringComPortNames() {
        String[] stringArgsComNames = new String[SerialPort.getCommPorts().length];
        for (int i = 0; i < SerialPort.getCommPorts().length; i++) {
            stringArgsComNames[i] = SerialPort.getCommPorts()[i].getSystemPortName();
        }
        return stringArgsComNames;
    }

    public List<String> getListPortNames() {
        List<String> stringList = new ArrayList<>();
        if (getStringComPortNames() != null)
            stringList = Arrays.asList(getStringComPortNames());
        return stringList;
    }

    public void setFieldsDisabled(ActionEvent event) {

        selectedValue4 = EDTypeBox.getSelectionModel().getSelectedItem();
        selectedValue5 = PCTypeBox.getSelectionModel().getSelectedItem();

        if (selectedValue4.equals(TypeCntEnum.Modem_4wire.getCntName())) {
            EDLocalIPModem_4wire.setDisable(false);
            EDLocalPortModem_4wire.setDisable(false);
            EDRemoteIPModem_4wire.setDisable(false);
            EDRemotePortModem_4wire.setDisable(false);

            EDLocalIPRadiomodem.setDisable(true);
            EDLocalPortRadiomodem.setDisable(true);
            EDRemoteIPRadiomodem.setDisable(true);
            EDRemotePortRadiomodem.setDisable(true);

            EDLocalIP3G_4G.setDisable(true);
            EDLocalPort3G_4G.setDisable(true);
            EDRemoteIP3G_4G.setDisable(true);
            EDRemotePort3G_4G.setDisable(true);
        } else if (selectedValue4.equals(TypeCntEnum.Radiomodem.getCntName())) {
            EDLocalIPModem_4wire.setDisable(true);
            EDLocalPortModem_4wire.setDisable(true);
            EDRemoteIPModem_4wire.setDisable(true);
            EDRemotePortModem_4wire.setDisable(true);

            EDLocalIPRadiomodem.setDisable(false);
            EDLocalPortRadiomodem.setDisable(false);
            EDRemoteIPRadiomodem.setDisable(false);
            EDRemotePortRadiomodem.setDisable(false);

            EDLocalIP3G_4G.setDisable(true);
            EDLocalPort3G_4G.setDisable(true);
            EDRemoteIP3G_4G.setDisable(true);
            EDRemotePort3G_4G.setDisable(true);
        } else if (selectedValue4.equals(TypeCntEnum.Router_3G_4G.getCntName())) {
            EDLocalIPModem_4wire.setDisable(true);
            EDLocalPortModem_4wire.setDisable(true);
            EDRemoteIPModem_4wire.setDisable(true);
            EDRemotePortModem_4wire.setDisable(true);

            EDLocalIPRadiomodem.setDisable(true);
            EDLocalPortRadiomodem.setDisable(true);
            EDRemoteIPRadiomodem.setDisable(true);
            EDRemotePortRadiomodem.setDisable(true);

            EDLocalIP3G_4G.setDisable(false);
            EDLocalPort3G_4G.setDisable(false);
            EDRemoteIP3G_4G.setDisable(false);
            EDRemotePort3G_4G.setDisable(false);
        }
        if (selectedValue5.equals(TypeCntEnum.Modem_4wire.getCntName())) {
            PCRemoteIPModem_4wire.setDisable(false);
            PCRemotePortModem_4wire.setDisable(false);

            PCRemoteIPRadiomodem.setDisable(true);
            PCRemotePortRadiomodem.setDisable(true);
        } else if (selectedValue5.equals(TypeCntEnum.Radiomodem.getCntName())) {
            PCRemoteIPModem_4wire.setDisable(true);
            PCRemotePortModem_4wire.setDisable(true);

            PCRemoteIPRadiomodem.setDisable(false);
            PCRemotePortRadiomodem.setDisable(false);
        }
    }

    public void setAdjustmentLevel(ActionEvent event) {

        selectedValue11 = AdjustmentLevelBox.getSelectionModel().getSelectedItem();

        if (selectedValue11.equals(AdjustmentLevelEnum.AUTO.getAccessName())) {
            MGCFactorLabel.setVisible(false);
            MGCFactor.setVisible(false);
            AGLLevel.setVisible(true);
            AGLLevelLabel.setVisible(true);
            SoundGrid.getRowConstraints().get(2).setMaxHeight(0);
            SoundGrid.getRowConstraints().get(2).setMinHeight(0);
            SoundGrid.getRowConstraints().get(3).setMaxHeight(30);
            SoundGrid.getRowConstraints().get(3).setMinHeight(30);
        } else if (selectedValue11.equals(AdjustmentLevelEnum.MANUAL.getAccessName())) {
            MGCFactorLabel.setVisible(true);
            MGCFactor.setVisible(true);
            AGLLevel.setVisible(false);
            AGLLevelLabel.setVisible(false);
            SoundGrid.getRowConstraints().get(2).setMaxHeight(30);
            SoundGrid.getRowConstraints().get(2).setMinHeight(30);
            SoundGrid.getRowConstraints().get(3).setMaxHeight(0);
            SoundGrid.getRowConstraints().get(3).setMinHeight(0);
        }
    }

///////////////////GETTERS AND SETTERS///////////////////

    public ComboBox<String> getAWSBox() {
        return AWSBox;
    }

    public void setAWSBox(ComboBox<String> AWSBox) {
        this.AWSBox = AWSBox;
    }

    public ComboBox<String> getAccessBox() {
        return AccessBox;
    }

    public void setAccessBox(ComboBox<String> accessBox) {
        AccessBox = accessBox;
    }

    public ComboBox<String> getLanguageBox() {
        return LanguageBox;
    }

    public void setLanguageBox(ComboBox<String> languageBox) {
        LanguageBox = languageBox;
    }

    public ComboBox<String> getCoordinateViewBox() {
        return CoordinateViewBox;
    }

    public void setCoordinateViewBox(ComboBox<String> coordinateViewBox) {
        CoordinateViewBox = coordinateViewBox;
    }

    public TextField getDBIPAddress() {
        return DBIPAddress;
    }

    public void setDBIPAddress(TextField DBIPAddress) {
        this.DBIPAddress = DBIPAddress;
    }

    public TextField getDBPort() {
        return DBPort;
    }

    public void setDBPort(TextField DBPort) {
        this.DBPort = DBPort;
    }

    public TextField getDFIPAddress() {
        return DFIPAddress;
    }

    public void setDFIPAddress(TextField DFIPAddress) {
        this.DFIPAddress = DFIPAddress;
    }

    public TextField getDFPort() {
        return DFPort;
    }

    public void setDFPort(TextField DFPort) {
        this.DFPort = DFPort;
    }

    public TextField getEDLocalIPModem_4wire() {
        return EDLocalIPModem_4wire;
    }

    public void setEDLocalIPModem_4wire(TextField EDLocalIPModem_4wire) {
        this.EDLocalIPModem_4wire = EDLocalIPModem_4wire;
    }

    public TextField getEDLocalPortModem_4wire() {
        return EDLocalPortModem_4wire;
    }

    public void setEDLocalPortModem_4wire(TextField EDLocalPortModem_4wire) {
        this.EDLocalPortModem_4wire = EDLocalPortModem_4wire;
    }

    public TextField getEDRemoteIPModem_4wire() {
        return EDRemoteIPModem_4wire;
    }

    public void setEDRemoteIPModem_4wire(TextField EDRemoteIPModem_4wire) {
        this.EDRemoteIPModem_4wire = EDRemoteIPModem_4wire;
    }

    public TextField getEDRemotePortModem_4wire() {
        return EDRemotePortModem_4wire;
    }

    public void setEDRemotePortModem_4wire(TextField EDRemotePortModem_4wire) {
        this.EDRemotePortModem_4wire = EDRemotePortModem_4wire;
    }

    public TextField getEDLocalIPRadiomodem() {
        return EDLocalIPRadiomodem;
    }

    public void setEDLocalIPRadiomodem(TextField EDLocalIPRadiomodem) {
        this.EDLocalIPRadiomodem = EDLocalIPRadiomodem;
    }

    public TextField getEDLocalPortRadiomodem() {
        return EDLocalPortRadiomodem;
    }

    public void setEDLocalPortRadiomodem(TextField EDLocalPortRadiomodem) {
        this.EDLocalPortRadiomodem = EDLocalPortRadiomodem;
    }

    public TextField getEDRemoteIPRadiomodem() {
        return EDRemoteIPRadiomodem;
    }

    public void setEDRemoteIPRadiomodem(TextField EDRemoteIPRadiomodem) {
        this.EDRemoteIPRadiomodem = EDRemoteIPRadiomodem;
    }

    public TextField getEDRemotePortRadiomodem() {
        return EDRemotePortRadiomodem;
    }

    public void setEDRemotePortRadiomodem(TextField EDRemotePortRadiomodem) {
        this.EDRemotePortRadiomodem = EDRemotePortRadiomodem;
    }

    public ComboBox<String> getEDTypeBox() {
        return EDTypeBox;
    }

    public void setEDTypeBox(ComboBox<String> EDTypeBox) {
        this.EDTypeBox = EDTypeBox;
    }

    public TextField getEDLocalIP3G_4G() {
        return EDLocalIP3G_4G;
    }

    public void setEDLocalIP3G_4G(TextField EDLocalIP3G_4G) {
        this.EDLocalIP3G_4G = EDLocalIP3G_4G;
    }

    public TextField getEDLocalPort3G_4G() {
        return EDLocalPort3G_4G;
    }

    public void setEDLocalPort3G_4G(TextField EDLocalPort3G_4G) {
        this.EDLocalPort3G_4G = EDLocalPort3G_4G;
    }

    public TextField getEDRemoteIP3G_4G() {
        return EDRemoteIP3G_4G;
    }

    public void setEDRemoteIP3G_4G(TextField EDRemoteIP3G_4G) {
        this.EDRemoteIP3G_4G = EDRemoteIP3G_4G;
    }

    public TextField getEDRemotePort3G_4G() {
        return EDRemotePort3G_4G;
    }

    public void setEDRemotePort3G_4G(TextField EDRemotePort3G_4G) {
        this.EDRemotePort3G_4G = EDRemotePort3G_4G;
    }

    public TextField getPCRemoteIPRadiomodem() {
        return PCRemoteIPRadiomodem;
    }

    public void setPCRemoteIPRadiomodem(TextField PCRemoteIPRadiomodem) {
        this.PCRemoteIPRadiomodem = PCRemoteIPRadiomodem;
    }

    public TextField getPCRemotePortRadiomodem() {
        return PCRemotePortRadiomodem;
    }

    public void setPCRemotePortRadiomodem(TextField PCRemotePortRadiomodem) {
        this.PCRemotePortRadiomodem = PCRemotePortRadiomodem;
    }

    public TextField getPCRemoteIPModem_4wire() {
        return PCRemoteIPModem_4wire;
    }

    public void setPCRemoteIPModem_4wire(TextField PCRemoteIPModem_4wire) {
        this.PCRemoteIPModem_4wire = PCRemoteIPModem_4wire;
    }

    public TextField getPCRemotePortModem_4wire() {
        return PCRemotePortModem_4wire;
    }

    public ComboBox<String> getPCTypeBox() {
        return PCTypeBox;
    }

    public void setPCTypeBox(ComboBox<String> PCTypeBox) {
        this.PCTypeBox = PCTypeBox;
    }

    public void setPCRemotePortModem_4wire(TextField PCRemotePortModem_4wire) {
        this.PCRemotePortModem_4wire = PCRemotePortModem_4wire;
    }

    public ComboBox<String> getComBox() {
        return ComBox;
    }

    public void setComBox(ComboBox<String> comBox) {
        ComBox = comBox;
    }

    public ComboBox<String> getSpeedBox() {
        return SpeedBox;
    }

    public void setSpeedBox(ComboBox<String> speedBox) {
        SpeedBox = speedBox;
    }


    public VBox getvBox() {
        return vBox;
    }

    public void setvBox(VBox vBox) {
        this.vBox = vBox;
    }

    public TextField getMapPath() {
        return MapPath;
    }

    public void setMapPath(TextField mapPath) {
        MapPath = mapPath;
    }

    public TextField getMatrixPath() {
        return MatrixPath;
    }

    public void setMatrixPath(TextField matrixPath) {
        MatrixPath = matrixPath;
    }

    public TextField getMapDFPath() {
        return MapDFPath;
    }

    public void setMapDFPath(TextField mapDFPath) {
        MapDFPath = mapDFPath;
    }

    public TextField getMatrixDFPath() {
        return MatrixDFPath;
    }

    public void setMatrixDFPath(TextField matrixDFPath) {
        MatrixDFPath = matrixDFPath;
    }

    public TextField getDistanceBearingLine() {
        return DistanceBearingLine;
    }

    public void setDistanceBearingLine(TextField distanceBearingLine) {
        DistanceBearingLine = distanceBearingLine;
    }

    public TextField getAltitude() {
        return Altitude;
    }

    public void setAltitude(TextField altitude) {
        Altitude = altitude;
    }

    public ComboBox<String> getFFTResolutionBox() {
        return FFTResolutionBox;
    }

    public void setFFTResolutionBox(ComboBox<String> FFTResolutionBox) {
        this.FFTResolutionBox = FFTResolutionBox;
    }

    public TextField getJammingTime() {
        return JammingTime;
    }

    public void setJammingTime(TextField jammingTime) {
        JammingTime = jammingTime;
    }

    public TextField getNoPA() {
        return NoPA;
    }

    public void setNoPA(TextField noPA) {
        NoPA = noPA;
    }

    public TextField getNoBA() {
        return NoBA;
    }

    public void setNoBA(TextField noBA) {
        NoBA = noBA;
    }

    public TextField getCourseAngle() {
        return CourseAngle;
    }

    public void setCourseAngle(TextField courseAngle) {
        CourseAngle = courseAngle;
    }

    public TextField getAdaptiveThresholdFhss() {
        return AdaptiveThresholdFhss;
    }

    public void setAdaptiveThresholdFhss(TextField adaptiveThresholdFhss) {
        AdaptiveThresholdFhss = adaptiveThresholdFhss;
    }

    public TextField getLSRLTime() {
        return LSRLTime;
    }

    public void setLSRLTime(TextField LSRLTime) {
        this.LSRLTime = LSRLTime;
    }

    public TextField getPriority() {
        return Priority;
    }

    public void setPriority(TextField priority) {
        Priority = priority;
    }

    public TextField getThreshold() {
        return Threshold;
    }

    public void setThreshold(TextField threshold) {
        Threshold = threshold;
    }

    public TextField getRadiationTime() {
        return RadiationTime;
    }

    public void setRadiationTime(TextField radiationTime) {
        RadiationTime = radiationTime;
    }

    public TextField getFHSSRadiationTime() {
        return FHSSRadiationTime;
    }

    public void setFHSSRadiationTime(TextField FHSSRadiationTime) {
        this.FHSSRadiationTime = FHSSRadiationTime;
    }

    public TextField getChannelLitter() {
        return ChannelLitter;
    }

    public void setChannelLitter(TextField channelLitter) {
        ChannelLitter = channelLitter;
    }

    public TextField getIRILitter() {
        return IRILitter;
    }

    public void setIRILitter(TextField IRILitter) {
        this.IRILitter = IRILitter;
    }

    public TextField getSearchSector() {
        return SearchSector;
    }

    public void setSearchSector(TextField searchSector) {
        SearchSector = searchSector;
    }

    public TextField getSearchTime() {
        return SearchTime;
    }

    public void setSearchTime(TextField searchTime) {
        SearchTime = searchTime;
    }

    public TextField getLatitude() {
        return Latitude;
    }

    public void setLatitude(TextField latitude) {
        Latitude = latitude;
    }

    public TextField getLongtitude() {
        return Longtitude;
    }

    public void setLongtitude(TextField longtitude) {
        Longtitude = longtitude;
    }

    public TitledPane getDBTitledPane() {
        return DBTitledPane;
    }

    public void setDBTitledPane(TitledPane DBTitledPane) {
        this.DBTitledPane = DBTitledPane;
    }

    public TitledPane getDFTitledPane() {
        return DFTitledPane;
    }

    public void setDFTitledPane(TitledPane DFTitledPane) {
        this.DFTitledPane = DFTitledPane;
    }

    public TitledPane getEDTitledPane() {
        return EDTitledPane;
    }

    public void setEDTitledPane(TitledPane EDTitledPane) {
        this.EDTitledPane = EDTitledPane;
    }

    public TitledPane getPCTitledPane() {
        return PCTitledPane;
    }

    public void setPCTitledPane(TitledPane PCTitledPane) {
        this.PCTitledPane = PCTitledPane;
    }

    public TitledPane getBPSSTitledPane() {
        return BPSSTitledPane;
    }

    public void setBPSSTitledPane(TitledPane BPSSTitledPane) {
        this.BPSSTitledPane = BPSSTitledPane;
    }

    public GridPane getPCGridPane() {
        return PCGridPane;
    }

    public void setPCGridPane(GridPane PCGridPane) {
        this.PCGridPane = PCGridPane;
    }

    public CheckBox getTestCheck() {
        return TestCheck;
    }

    public void setTestCheck(CheckBox testCheck) {
        TestCheck = testCheck;
    }

    public Label getLabelModem_4wire() {
        return LabelModem_4wire;
    }

    public void setLabelModem_4wire(Label labelModem_4wire) {
        LabelModem_4wire = labelModem_4wire;
    }

    public Label getLabelRadiomodem() {
        return LabelRadiomodem;
    }

    public void setLabelRadiomodem(Label labelRadiomodem) {
        LabelRadiomodem = labelRadiomodem;
    }

    public Label getLabelPCRemoteIPModem_4wire() {
        return LabelPCRemoteIPModem_4wire;
    }

    public void setLabelPCRemoteIPModem_4wire(Label labelPCRemoteIPModem_4wire) {
        LabelPCRemoteIPModem_4wire = labelPCRemoteIPModem_4wire;
    }

    public Label getLabelPCRemotePortModem_4wire() {
        return LabelPCRemotePortModem_4wire;
    }

    public void setLabelPCRemotePortModem_4wire(Label labelPCRemotePortModem_4wire) {
        LabelPCRemotePortModem_4wire = labelPCRemotePortModem_4wire;
    }

    public Label getLabelPCRemoteIPRadiomodem() {
        return LabelPCRemoteIPRadiomodem;
    }

    public void setLabelPCRemoteIPRadiomodem(Label labelPCRemoteIPRadiomodem) {
        LabelPCRemoteIPRadiomodem = labelPCRemoteIPRadiomodem;
    }

    public Label getLabelPCRemotePortRadiomodem() {
        return LabelPCRemotePortRadiomodem;
    }

    public void setLabelPCRemotePortRadiomodem(Label labelPCRemotePortRadiomodem) {
        LabelPCRemotePortRadiomodem = labelPCRemotePortRadiomodem;
    }

    public PropertyControllerForAdmin getPropertyControllerForAdmin() {
        return propertyControllerForAdmin;
    }

    public void setPropertyControllerForAdmin(PropertyControllerForAdmin propertyControllerForAdmin) {
        this.propertyControllerForAdmin = propertyControllerForAdmin;
    }

    public GlobalPropertiesModel getGlobalPropertiesModel() {
        return globalPropertiesModel;
    }

    public void setGlobalPropertiesModel(GlobalPropertiesModel globalPropertiesModel) {
        this.globalPropertiesModel = globalPropertiesModel;
    }

    public Coord getCoord() {
        return coord;
    }

    public void setCoord(Coord coord) {
        this.coord = coord;
    }

    public boolean isFlag1() {
        return flag1;
    }

    public void setFlag1(boolean flag1) {
        this.flag1 = flag1;
    }

    public boolean isFlag2() {
        return flag2;
    }

    public void setFlag2(boolean flag2) {
        this.flag2 = flag2;
    }

    public boolean isFlag3() {
        return flag3;
    }

    public void setFlag3(boolean flag3) {
        this.flag3 = flag3;
    }

    public String getSelectedValue() {
        return selectedValue;
    }

    public void setSelectedValue(String selectedValue) {
        this.selectedValue = selectedValue;
    }

    public String getSelectedValue1() {
        return selectedValue1;
    }

    public void setSelectedValue1(String selectedValue1) {
        this.selectedValue1 = selectedValue1;
    }

    public String getSelectedValue2() {
        return selectedValue2;
    }

    public void setSelectedValue2(String selectedValue2) {
        this.selectedValue2 = selectedValue2;
    }

    public String getSelectedValue3() {
        return selectedValue3;
    }

    public void setSelectedValue3(String selectedValue3) {
        this.selectedValue3 = selectedValue3;
    }

    public String getSelectedValue4() {
        return selectedValue4;
    }

    public void setSelectedValue4(String selectedValue4) {
        this.selectedValue4 = selectedValue4;
    }

    public String getSelectedValue5() {
        return selectedValue5;
    }

    public void setSelectedValue5(String selectedValue5) {
        this.selectedValue5 = selectedValue5;
    }

    public String getSelectedValue6() {
        return selectedValue6;
    }

    public void setSelectedValue6(String selectedValue6) {
        this.selectedValue6 = selectedValue6;
    }

    public String getSelectedValue7() {
        return selectedValue7;
    }

    public void setSelectedValue7(String selectedValue7) {
        this.selectedValue7 = selectedValue7;
    }

    public String getSelectedValue20() {
        return selectedValue20;
    }

    public void setSelectedValue20(String selectedValue20) {
        this.selectedValue20 = selectedValue20;
    }

    public List<IPropertiesEvent> getListeners() {
        return listeners;
    }

    public void setListeners(List<IPropertiesEvent> listeners) {
        this.listeners = listeners;
    }


//LOCAL

    public void setLocalSettings(LocalSettings localSettingsold) {
        this.localSettings = localSettingsold;
        UpdLocal(localSettingsold);
    }

    public LocalSettings getLocalSettings() {
        return localSettings;
    }

//GLOBAL

    public void setGlobalSettings(GlobalPropertiesModel globalPropertiesModel) {
        this.globalPropertiesModel = globalPropertiesModel;
        UpdGlobal(globalPropertiesModel);
    }
}
