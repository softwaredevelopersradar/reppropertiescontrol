package PropertiesControl;

import java.io.IOException;

public interface IAdminWindowEvent {
    void AdminWindowAccessGranted() throws IOException;
    void AdminWindowAccessDenied() throws  IOException;
}
